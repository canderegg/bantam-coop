# Import the required modules
import unittest, os
from coop import *

# A class to validate against the current repository
# Allows registry validation before merge
class RepoRegistry(Registry):

  # Create a new RepoRegistry
  def __init__(self, root):
    self.root = root
    self.index = None

  # Re-map a URL into a local file path
  def remap(self, path):
    if not path.startswith(self.root):
      return path

    path = path.replace(self.root, '')
    result = 'public'
    for c in path.split('/'):
      result = os.path.join(result, c)

    return result

  # Get the data from the repository
  def get(self, path):

    path = self.remap(path)

    if path.startswith('http'):
      return ''

    with open(path, 'r') as f:
      return f.read()


# A class to smoke test the module registry
class ValidateRegistry(unittest.TestCase):

  # Test that the module index is properly formed
  def test_module_index_formation(self):
    r = RepoRegistry(REGISTRY_PATH)

    index = r.module_index()

    self.assertEqual(dict, type(index))

  # Test that each module in the index is properly formed
  def test_module_formation(self):
    r = RepoRegistry(REGISTRY_PATH)

    index = r.module_index()

    # Loop over all modules in the index to check that they are parseable
    for mdl in index:
      p = r.module(mdl)
      self.assertEqual('RegistryEntry', p.__class__.__name__)

      for v in p.versions:
        for f in v.files:
          if f.startswith(r.root):
            self.assertTrue(IOOperations.file_exists(r.remap(f)))



# Execute the unit tests
if __name__ == '__main__':
    unittest.main()
