# Import the required modules
from json import dumps
import unittest, sys, os
from os.path import join
from shutil import copyfile
from coop import *

# A class to mock the registry, to allow unit testing with known registry info
class MockRegistry(Registry):

  # Initialize a new unit testing registry
  def __init__(self, root):
    self.root = root
    self.index = None

  # Override the get command to read registry information from local storage
  def get(self, url):
    path = join('testing',url.split('/')[-1])
    try:
      with open(path, 'r') as f:
        return f.read()
    except:
      raise IOError('Unable to find module')


# A class to test the Dependency class
class TestDependency(unittest.TestCase):

  # Test the module name is properly parsed
  def test_module_name(self):
    d = Dependency('bantam','1.0')
    self.assertEqual('bantam', d.module_name)

    d = Dependency('BANTAM','1.0')
    self.assertEqual('bantam', d.module_name)

  # Test the version string is properly parsed
  def test_version_string(self):
    d = Dependency('bantam','1.0.0')
    self.assertEqual('1.0.0', d.version_string)

    d = Dependency('bantam','+1.2.6')
    self.assertEqual('+1.2.6', d.version_string)

    d = Dependency('bantam','^2')
    self.assertEqual('^2', d.version_string)

  # Test the minimum version is properly computed
  def test_min_version(self):
    v = Dependency('bantam','1.0.0').get_min_version()
    self.assertEqual(3, len(v))
    self.assertEqual(1,v[0])
    self.assertEqual(0,v[1])
    self.assertEqual(0,v[2])

    v = Dependency('bantam','+1.2.6').get_min_version()
    self.assertEqual(3, len(v))
    self.assertEqual(1,v[0])
    self.assertEqual(2,v[1])
    self.assertEqual(6,v[2])

    v = Dependency('bantam','^2.0').get_min_version()
    self.assertEqual(2, len(v))
    self.assertEqual(2,v[0])
    self.assertEqual(0,v[1])

  # Test the maximum version is properly computed
  def test_max_version(self):
    v = Dependency('bantam','1.0.0').get_max_version()
    self.assertEqual(4, len(v))
    self.assertEqual(1,v[0])
    self.assertEqual(0,v[1])
    self.assertEqual(0,v[2])
    self.assertEqual(0,v[3])

    v = Dependency('bantam','+1.2.6').get_max_version()
    self.assertEqual(None, v)

    v = Dependency('bantam','^2.0').get_max_version()
    self.assertEqual(1, len(v))
    self.assertEqual(3,v[0])

  # Test the conversion of this Dependency to a string
  def test_str(self):
    d = Dependency('bantam','1.0.0')
    self.assertEqual('bantam@1.0.0', str(d))

    d = Dependency('bantam','+1.2.6')
    self.assertEqual('bantam@+1.2.6', str(d))

    d = Dependency('bantam','^2')
    self.assertEqual('bantam@^2', str(d))

  # Test the serialization of this Dependency
  def test_serialize(self):
    d = Dependency('bantam','1.0.0')
    self.assertEqual('"bantam": "1.0.0"', d.serialize())

    d = Dependency('bantam','+1.2.6')
    self.assertEqual('"bantam": "+1.2.6"', d.serialize())

    d = Dependency('bantam','^2')
    self.assertEqual('"bantam": "^2"', d.serialize())

  # Test the method to parse version strings into numeric versions
  def test_parse_version(self):
    v = Dependency.parse_version('1')
    self.assertEqual(1, len(v))
    self.assertEqual(1, v[0])

    v = Dependency.parse_version('2.0')
    self.assertEqual(2, len(v))
    self.assertEqual(2, v[0])
    self.assertEqual(0, v[1])

    v = Dependency.parse_version('1.7.5')
    self.assertEqual(3, len(v))
    self.assertEqual(1, v[0])
    self.assertEqual(7, v[1])
    self.assertEqual(5, v[2])

    v = Dependency.parse_version('3.18.6.21')
    self.assertEqual(4, len(v))
    self.assertEqual(3, v[0])
    self.assertEqual(18, v[1])
    self.assertEqual(6, v[2])
    self.assertEqual(21, v[3])

    self.assertEqual(None, Dependency.parse_version('null'))
    self.assertEqual(None, Dependency.parse_version('NULL'))

    with self.assertRaises(ValueError):
      Dependency.parse_version('1.15.a')

    with self.assertRaises(ValueError):
      Dependency.parse_version('+1.0')

    with self.assertRaises(ValueError):
      Dependency.parse_version('^1.13')

    with self.assertRaises(ValueError):
      Dependency.parse_version('1..9')

    with self.assertRaises(ValueError):
      Dependency.parse_version('toast')

  # Test the method to convert numeric versions back to version strings
  def test_version_string(self):
    self.assertEqual('1', Dependency.version_string([1]))
    self.assertEqual('2.0', Dependency.version_string([2,0]))
    self.assertEqual('1.7.5', Dependency.version_string([1,7,5]))
    self.assertEqual('3.18.6.21', Dependency.version_string([3,18,6,21]))
    self.assertEqual('null', Dependency.version_string(None))
    self.assertEqual('null', Dependency.version_string([]))


# A class to test the comparison of Dependency objects
class TestDependencyComparison(unittest.TestCase):

  # Test that equal Dependencies return 0 when compared
  def test_equality(self):
    self.assertEqual(0, Dependency.compare([1], [1]))
    self.assertEqual(0, Dependency.compare([1,0], [1,0]))
    self.assertEqual(0, Dependency.compare([1,0,0], [1,0,0]))
    self.assertEqual(0, Dependency.compare([1,0,0,3], [1,0,0,3]))
    self.assertEqual(0, Dependency.compare(None, None))

  # Test that ordered Dependecies return results less than 0 when compared
  def test_ordered(self):
    self.assertTrue(Dependency.compare([1,0,0,0], [2,0,0,0]) < 0)
    self.assertTrue(Dependency.compare([1,0,0,0], [1,1,0,0]) < 0)
    self.assertTrue(Dependency.compare([1,0,0,0], [1,0,1,0]) < 0)
    self.assertTrue(Dependency.compare([1,0,0,0], [1,0,0,1]) < 0)

    self.assertTrue(Dependency.compare([1,0,0,0], [2]) < 0)
    self.assertTrue(Dependency.compare([1,0,0,0], None) < 0)
    self.assertTrue(Dependency.compare([1,0,0], [1,0,0,0]) < 0)

    self.assertTrue(Dependency.compare([1,3,7], [3,2]) < 0)
    self.assertTrue(Dependency.compare([1,6,4], [1,15,0]) < 0)
    self.assertTrue(Dependency.compare([2,9,7], [2,9,7,8]) < 0)
    self.assertTrue(Dependency.compare([2,9,7], [2,10,6]) < 0)

  # Test that unordered Dependencies return results greater than 0 when compared
  def test_unordered(self):
    self.assertTrue(Dependency.compare([2,0,0,0], [1,0,0,0]) > 0)
    self.assertTrue(Dependency.compare([1,1,0,0], [1,0,0,0]) > 0)
    self.assertTrue(Dependency.compare([1,0,1,0], [1,0,0,0]) > 0)
    self.assertTrue(Dependency.compare([1,0,0,1], [1,0,0,0]) > 0)

    self.assertTrue(Dependency.compare([2], [1,0,0,0]) > 0)
    self.assertTrue(Dependency.compare(None, [1,0,0,0]) > 0)
    self.assertTrue(Dependency.compare([1,0,0,0], [1,0,0]) > 0)

    self.assertTrue(Dependency.compare([3,2], [1,3,7]) > 0)
    self.assertTrue(Dependency.compare([1,15,0], [1,6,4]) > 0)
    self.assertTrue(Dependency.compare([2,9,7,8], [2,9,7]) > 0)
    self.assertTrue(Dependency.compare([2,10,6], [2,9,7]) > 0)


# Strings to be used in testing Manifest parsing and serialization
default_manifest_json = dumps({
    'css_target': join('styles','dependencies.css'),
    'dependencies':{},
    'js_target': join('scripts','dependencies.js'),
    'module_directory': 'bantam_modules',
    'prefix_target': join('scripts','prefix-dependencies.js')
  }, sort_keys=True, indent=2)
modified_manifest_json = dumps({
    'css_target': join('public','styles','dependencies.css'),
    'dependencies':{
      'bantam': '^2.1',
      'bantam-tabs': '+1.0',
      'bantam-toc': '1.0'
    },
    'js_target': join('public','scripts','dependencies.js'),
    'module_directory': 'js_modules',
    'prefix_target': join('public','scripts','prefix-dependencies.js')
  }, sort_keys=True, indent=2)

# A class to test the Manifest object
class TestManifest(unittest.TestCase):

  # Test default values within the manifest file
  def test_defaults(self):
    m = Manifest()

    self.assertEqual('bantam_modules', m.module_directory)
    self.assertEqual(join('scripts','dependencies.js'), m.js_target)
    self.assertEqual(join('styles','dependencies.css'), m.css_target)
    self.assertEqual(join('scripts','prefix-dependencies.js'), m.prefix_target)

    self.assertEqual(0, len(m.dependencies))

  # Test serialization of the Manifest objects
  def test_serialization(self):
    m = Manifest()

    self.assertEqual(default_manifest_json, m.serialize())

    m.module_directory = 'js_modules'
    m.js_target = join('public', m.js_target)
    m.css_target = join('public', m.css_target)
    m.prefix_target = join('public', m.prefix_target)
    m.dependencies.append(Dependency('bantam','^2.1'))
    m.dependencies.append(Dependency('bantam-tabs','+1.0'))
    m.dependencies.append(Dependency('bantam-toc','1.0'))

    self.assertEqual(modified_manifest_json, m.serialize())

  # Test parsing JSON strings into Manifest objects
  def test_parsing(self):
    m = Manifest.loads(default_manifest_json)

    self.assertEqual('bantam_modules', m.module_directory)
    self.assertEqual(join('scripts','dependencies.js'), m.js_target)
    self.assertEqual(join('styles','dependencies.css'), m.css_target)
    self.assertEqual(join('scripts','prefix-dependencies.js'), m.prefix_target)
    self.assertEqual(0, len(m.dependencies))

    m = Manifest.loads(modified_manifest_json)

    self.assertEqual('js_modules', m.module_directory)
    self.assertEqual(join('public','scripts','dependencies.js'), m.js_target)
    self.assertEqual(join('public','styles','dependencies.css'), m.css_target)
    self.assertEqual(join('public','scripts','prefix-dependencies.js'), m.prefix_target)
    self.assertEqual(3, len(m.dependencies))

    d = [d for d in m.dependencies if d.module_name == 'bantam'][0]
    self.assertEqual('bantam', d.module_name)
    self.assertEqual('^2.1', d.version_string)
    d = [d for d in m.dependencies if d.module_name == 'bantam-tabs'][0]
    self.assertEqual('bantam-tabs', d.module_name)
    self.assertEqual('+1.0', d.version_string)
    d = [d for d in m.dependencies if d.module_name == 'bantam-toc'][0]
    self.assertEqual('bantam-toc', d.module_name)
    self.assertEqual('1.0', d.version_string)

  # Test that trying to load a Manifest from a non-existent file throws errors
  def test_load_errors(self):
    with self.assertRaises(IOError):
      m = Manifest.loadf('does-not-exist.json')


# A class to test the IOOperations library
class TestIOOperations(unittest.TestCase):

  # Test that the file existence method functions properly
  def test_file_exists(self):
    self.assertTrue(IOOperations.file_exists('coop.py'))
    self.assertFalse(IOOperations.file_exists(join('..','source')))
    self.assertFalse(IOOperations.file_exists('does-not-exist.test'))

  # Test that the directory existence method functions properly
  def test_directory_exists(self):
    self.assertTrue(IOOperations.directory_exists(join('..','source')))
    self.assertTrue(IOOperations.directory_exists(join('..','source') + os.path.sep))
    self.assertFalse(IOOperations.directory_exists('coop.py'))
    self.assertFalse(IOOperations.directory_exists('does-not-exist'))

  # Test that methods for making and removing directories function properly
  def test_directory_operations(self):
    self.assertFalse(IOOperations.directory_exists('test1'))
    IOOperations.mkdir('test1')
    self.assertTrue(IOOperations.directory_exists('test1'))

    self.assertFalse(IOOperations.directory_exists(join('test1','test2')))
    IOOperations.mkdir(join('test1','test2'))
    self.assertTrue(IOOperations.directory_exists(join('test1','test2')))

    with open(join('test1','file.txt'), 'w') as f:
      f.write('Stuff!')

    if sys.version_info[0] < 3:
      with self.assertRaises(OSError):
        IOOperations.rmdir('does-not-exist')
    else:
      with self.assertRaises(FileNotFoundError):
        IOOperations.rmdir('does-not-exist')

    with self.assertRaises(OSError):
      IOOperations.rmdir('test1')

    with self.assertRaises(OSError):
      IOOperations.rmdir('test1', recursive=False)

    self.assertTrue(IOOperations.directory_exists(join('test1','test2')))
    IOOperations.rmdir(join('test1','test2'), recursive=False)
    self.assertFalse(IOOperations.directory_exists(join('test1','test2')))

    IOOperations.mkdir(join('test1','test2'))
    self.assertTrue(IOOperations.directory_exists('test1'))
    self.assertTrue(IOOperations.file_exists(join('test1','file.txt')))
    self.assertTrue(IOOperations.directory_exists(join('test1','test2')))
    IOOperations.rmdir('test1', recursive=True)
    self.assertFalse(IOOperations.file_exists(join('test1','file.txt')))
    self.assertFalse(IOOperations.directory_exists(join('test1','test2')))
    self.assertFalse(IOOperations.directory_exists('test1'))

  # Test that the method to read data from a URL functions properly
  def test_readurl(self):
    with self.assertRaises(IOError):
      IOOperations.readurl('https://canderegg.gitlab.io/bantam-coop/does-not-exist.json')

    self.assertEqual('{ "test_success": true }\n', IOOperations.readurl('https://canderegg.gitlab.io/bantam-coop/test.json'))

  # Test that the method to get a list of files by location and type
  def test_get_files_of_type(self):
    files = IOOperations.get_files_of_type('testing','.json')

    self.assertEqual(7, len(files))
    self.assertTrue('modules.json' in files)
    self.assertTrue('bantam.json' in files)
    self.assertTrue('p-bantam1.json' in files)
    self.assertTrue('p-bantam11.json' in files)
    self.assertTrue('p-chained.json' in files)
    self.assertTrue('p-multi.json' in files)
    self.assertTrue('p-circular.json' in files)

    files = IOOperations.get_files_of_type('testing','.docx')

    self.assertEqual(0, len(files))


# A class to test the Registry object
class TestRegistry(unittest.TestCase):

  # Test the method to get data from the registry
  def test_get(self):
    r = Registry(REGISTRY_PATH)
    self.assertEqual('{ "test_success": true }\n', r.get('https://canderegg.gitlab.io/bantam-coop/test.json'))

  # Test the method to return the registry module index
  def test_module_index(self):
    r = MockRegistry(REGISTRY_PATH)
    index = r.module_index()

    self.assertEqual(dict, type(index))
    self.assertEqual(6, len(index))
    self.assertTrue('bantam' in index)
    self.assertTrue('p-bantam1' in index)
    self.assertTrue('p-bantam11' in index)
    self.assertTrue('p-chained' in index)
    self.assertTrue('p-circular' in index)
    self.assertTrue('p-multi' in index)

  # Test the method to return a specific module from the registry
  def test_module(self):
    r = MockRegistry(REGISTRY_PATH)

    with self.assertRaises(ValueError):
      r.module('does-not-exist')

    p = r.module('p-bantam11')

    self.assertEqual('p-bantam11', p.module_name)
    self.assertEqual('A module with a dependency on bantam@+1.1', p.module_desc)
    self.assertEqual('Authors Name', p.author)
    self.assertEqual('documentation_url',p.documentation)
    self.assertEqual('license_url',p.license)
    self.assertEqual('repository_url',p.repository)

    self.assertEqual(1, len(p.dependencies))
    strings = [str(d) for d in p.dependencies]
    strings.sort()
    self.assertEqual('bantam@+1.1', strings[0])

    self.assertEqual(1, len(p.versions))
    self.assertEqual('1.0', p.versions[0].version_string)
    self.assertEqual(1, len(p.versions[0].files))
    self.assertEqual('p-bantam11.js', p.versions[0].files[0])


# A class to test the module Version objects
class TestVersion(unittest.TestCase):

  # Test the initialization of a new Version object
  def test_init(self):
    v1 = Version('1.0', ['fileA', 'fileB', 'fileC'])
    v2 = Version('1.19.6', ['fileD'])
    v3 = Version('2.17', ['fileE', 'fileF'])

    self.assertEqual('1.0', v1.version_string)
    self.assertEqual(2, len(v1.numeric_version))
    self.assertEqual(1, v1.numeric_version[0])
    self.assertEqual(0, v1.numeric_version[1])
    self.assertEqual(3, len(v1.files))
    self.assertEqual('fileA', v1.files[0])
    self.assertEqual('fileB', v1.files[1])
    self.assertEqual('fileC', v1.files[2])

    self.assertEqual('1.19.6', v2.version_string)
    self.assertEqual(3, len(v2.numeric_version))
    self.assertEqual(1, v2.numeric_version[0])
    self.assertEqual(19, v2.numeric_version[1])
    self.assertEqual(6, v2.numeric_version[2])
    self.assertEqual(1, len(v2.files))
    self.assertEqual('fileD', v2.files[0])

    self.assertEqual('2.17', v3.version_string)
    self.assertEqual(2, len(v3.numeric_version))
    self.assertEqual(2, v3.numeric_version[0])
    self.assertEqual(17, v3.numeric_version[1])
    self.assertEqual(2, len(v3.files))
    self.assertEqual('fileE', v3.files[0])
    self.assertEqual('fileF', v3.files[1])

  # Test the equality comparison of Version objects
  def test_comparison_equality(self):
    v1 = Version('1.0', ['fileA', 'fileB', 'fileC'])
    v2a = Version('1.19.6', ['fileD'])
    v2b = Version('1.19.6', ['some other file'])
    v3 = Version('2.17', ['fileE', 'fileF'])

    self.assertFalse(v1 == v2a)
    self.assertFalse(v1 == v2b)
    self.assertFalse(v1 == v3)
    self.assertFalse(v2a == v1)
    self.assertFalse(v2b == v1)
    self.assertFalse(v3 == v1)

    self.assertTrue(v2a == v2b)
    self.assertFalse(v2a == v3)
    self.assertTrue(v2b == v2a)
    self.assertFalse(v3 == v2a)

    self.assertFalse(v2b == v3)
    self.assertFalse(v3 == v2b)

  # Test the less-than comparison of Version objects
  def test_comparison_lessthan(self):
    v1 = Version('1.0', ['fileA', 'fileB', 'fileC'])
    v2a = Version('1.19.6', ['fileD'])
    v2b = Version('1.19.6', ['some other file'])
    v3 = Version('2.17', ['fileE', 'fileF'])

    self.assertTrue(v1 < v2a)
    self.assertTrue(v1 < v2b)
    self.assertTrue(v1 < v3)
    self.assertFalse(v2a < v1)
    self.assertFalse(v2b < v1)
    self.assertFalse(v3 < v1)

    self.assertFalse(v2a < v2b)
    self.assertTrue(v2a < v3)
    self.assertFalse(v2b < v2a)
    self.assertFalse(v3 < v2a)

    self.assertTrue(v2b < v3)
    self.assertFalse(v3 < v2b)

  # Test that Version objects can be appropriately sorted
  def test_comparison_sorting(self):
    v1 = Version('1.0', ['fileA', 'fileB', 'fileC'])
    v2 = Version('1.19.6', ['fileD'])
    v3 = Version('2.17', ['fileE', 'fileF'])
    v4 = Version('2.17.0', ['fileG', 'fileH'])

    versions = [v4, v3, v2, v1]
    versions.sort()
    self.assertEqual(v1, versions[0])
    self.assertEqual(v2, versions[1])
    self.assertEqual(v3, versions[2])
    self.assertEqual(v4, versions[3])

    versions = [v1, v3, v2, v4]
    versions.sort()
    self.assertEqual(v1, versions[0])
    self.assertEqual(v2, versions[1])
    self.assertEqual(v3, versions[2])
    self.assertEqual(v4, versions[3])

    versions = [v3, v2, v1, v4]
    versions.sort()
    self.assertEqual(v1, versions[0])
    self.assertEqual(v2, versions[1])
    self.assertEqual(v3, versions[2])
    self.assertEqual(v4, versions[3])


# A class to test the details of a single RegistryEntry object
class TestRegistryEntry(unittest.TestCase):

  # Test the initialization of a new RegistryEntry object
  def test_init(self):
    data = {
      'module-name': 'name',
      'module-desc': 'description',
      'author': 'Authors Name',
      'documentation': 'documentation_url',
      'license': 'license_url',
      'repository': 'repository_url',
      'dependencies': {
        'dependency-1': '+1.0.0',
        'dependency-2': '1.7'
      },
      'versions': {
        '1.1': [
          'file1',
          'file2'
        ],
        '1.0': [
          'file3',
          'file4'
        ]
      }
    }

    p = RegistryEntry(data)

    self.assertEqual('name', p.module_name)
    self.assertEqual('description', p.module_desc)
    self.assertEqual('Authors Name', p.author)
    self.assertEqual('documentation_url',p.documentation)
    self.assertEqual('license_url',p.license)
    self.assertEqual('repository_url',p.repository)

    self.assertEqual(2, len(p.dependencies))
    strings = [str(d) for d in p.dependencies]
    strings.sort()
    self.assertEqual('dependency-1@+1.0.0', strings[0])
    self.assertEqual('dependency-2@1.7', strings[1])

    self.assertEqual(2, len(p.versions))
    self.assertEqual('1.0', p.versions[0].version_string)
    self.assertEqual(2, len(p.versions[0].files))
    self.assertEqual('file3', p.versions[0].files[0])
    self.assertEqual('file4', p.versions[0].files[1])
    self.assertEqual('1.1', p.versions[1].version_string)
    self.assertEqual(2, len(p.versions[1].files))
    self.assertEqual('file1', p.versions[1].files[0])
    self.assertEqual('file2', p.versions[1].files[1])

  # Test the method to get version files for the specified module Version
  def test_get_version_files(self):
    data = {
      'module-name': 'name',
      'module-desc': 'description',
      'author': 'Authors Name',
      'documentation': 'documentation_url',
      'license': 'license_url',
      'repository': 'repository_url',
      'dependencies': {
        'dependency-1': '+1.0.0',
        'dependency-2': '1.7'
      },
      'versions': {
        '1.1': [
          'file1',
          'file2'
        ],
        '1.0': [
          'file3',
          'file4'
        ]
      }
    }

    p = RegistryEntry(data)

    f = p.get_version_files('1.0')
    self.assertEqual(2, len(f))
    self.assertEqual('file3', f[0])
    self.assertEqual('file4', f[1])

    f = p.get_version_files('1.1')
    self.assertEqual(2, len(f))
    self.assertEqual('file1', f[0])
    self.assertEqual('file2', f[1])

    f = p.get_version_files('1.2')
    self.assertEqual(0, len(f))

  # Test the method to parse a RegistryEntry from a JSON string
  def test_loads(self):
    data = '{\n\t"module-name": "name",\n\t"module-desc": "description",\n\t"author": "Authors Name",\n\t"documentation": "documentation_url",\n\t"license": "license_url",\n\t"repository": "repository_url",\n\t"dependencies": {\n\t\t"dependency-1": "+1.0.0",\n\t\t"dependency-2": "1.7"\n\t},\n\t"versions": {\n\t\t"1.1": [\n\t\t\t"file1",\n\t\t\t"file2"\n\t\t],\n\t\t"1.0": [\n\t\t\t"file3",\n\t\t\t"file4"\n\t\t]\n\t}\n}'

    p = RegistryEntry.loads(data)

    self.assertEqual('name', p.module_name)
    self.assertEqual('description', p.module_desc)
    self.assertEqual('Authors Name', p.author)
    self.assertEqual('documentation_url',p.documentation)
    self.assertEqual('license_url',p.license)
    self.assertEqual('repository_url',p.repository)

    self.assertEqual(2, len(p.dependencies))
    strings = [str(d) for d in p.dependencies]
    strings.sort()
    self.assertEqual('dependency-1@+1.0.0', strings[0])
    self.assertEqual('dependency-2@1.7', strings[1])

    self.assertEqual(2, len(p.versions))
    self.assertEqual('1.0', p.versions[0].version_string)
    self.assertEqual(2, len(p.versions[0].files))
    self.assertEqual('file3', p.versions[0].files[0])
    self.assertEqual('file4', p.versions[0].files[1])
    self.assertEqual('1.1', p.versions[1].version_string)
    self.assertEqual(2, len(p.versions[1].files))
    self.assertEqual('file1', p.versions[1].files[0])
    self.assertEqual('file2', p.versions[1].files[1])

  # Test the method to add a new constraint to a RegistryEntry
  def test_add_constraint(self):
    r = MockRegistry(REGISTRY_PATH)
    p = r.module('bantam')

    self.assertEqual('bantam', p.module_name)
    self.assertEqual(5, len(p.versions))

    self.assertEqual(2, len(p.min_version))
    self.assertEqual(1, p.min_version[0])
    self.assertEqual(0, p.min_version[1])
    self.assertEqual(None, p.max_version)

    p.add_constraint(Dependency('not-relevant', '1.0.4'))

    self.assertEqual(2, len(p.min_version))
    self.assertEqual(1, p.min_version[0])
    self.assertEqual(0, p.min_version[1])
    self.assertEqual(None, p.max_version)

    p.add_constraint(Dependency('bantam', '+1.0'))

    self.assertEqual(2, len(p.min_version))
    self.assertEqual(1, p.min_version[0])
    self.assertEqual(0, p.min_version[1])
    self.assertEqual(None, p.max_version)

    p.add_constraint(Dependency('bantam','1.1+'))

    self.assertEqual(2, len(p.min_version))
    self.assertEqual(1, p.min_version[0])
    self.assertEqual(1, p.min_version[1])
    self.assertEqual(None, p.max_version)

    p.add_constraint(Dependency('bantam','^1.1.4'))

    self.assertEqual(3, len(p.min_version))
    self.assertEqual(1, p.min_version[0])
    self.assertEqual(1, p.min_version[1])
    self.assertEqual(4, p.min_version[2])
    self.assertEqual(1, len(p.max_version))
    self.assertEqual(2, p.max_version[0])

    p.add_constraint(Dependency('bantam','2.1'))

    self.assertEqual(2, len(p.min_version))
    self.assertEqual(2, p.min_version[0])
    self.assertEqual(1, p.min_version[1])
    self.assertEqual(1, len(p.max_version))
    self.assertEqual(2, p.max_version[0])

  # Test the method to compute the best install version of a RegistryEntry
  def test_get_install_version(self):
    r = MockRegistry(REGISTRY_PATH)
    p = r.module('bantam')

    self.assertEqual('bantam', p.module_name)
    self.assertEqual(5, len(p.versions))

    self.assertEqual('2.1', p.get_install_version())

    p.add_constraint(Dependency('not-relevant', '1.0.4'))
    self.assertEqual('2.1', p.get_install_version())

    p.add_constraint(Dependency('bantam', '+1.0'))
    self.assertEqual('2.1', p.get_install_version())

    p.add_constraint(Dependency('bantam','1.1+'))
    self.assertEqual('2.1', p.get_install_version())

    p.add_constraint(Dependency('bantam','^1.1.4'))
    self.assertEqual('1.2', p.get_install_version())

    p.add_constraint(Dependency('bantam','1.2'))
    self.assertEqual('1.2', p.get_install_version())

    p.add_constraint(Dependency('bantam','2.1'))
    self.assertEqual(None, p.get_install_version())


# A class to test the LockEntry object
class TestLockEntry(unittest.TestCase):

  # Test the method to build a LockEntry from a RegistryEntry
  def test_from_module(self):
    r = MockRegistry(REGISTRY_PATH)

    entry = LockEntry.from_module(r.module('bantam'))
    self.assertEqual('bantam', entry.module_name)
    self.assertEqual('1.0', entry.min_version)
    self.assertEqual('null', entry.max_version)
    self.assertEqual('2.1', entry.resolved)
    self.assertEqual(0, len(entry.dependencies))

    entry = LockEntry.from_module(r.module('p-bantam1'))
    self.assertEqual('p-bantam1', entry.module_name)
    self.assertEqual('1.0', entry.min_version)
    self.assertEqual('null', entry.max_version)
    self.assertEqual('1.0', entry.resolved)
    self.assertEqual(1, len(entry.dependencies))
    self.assertTrue('bantam' in entry.dependencies)

    entry = LockEntry.from_module(r.module('p-multi'))
    self.assertEqual('p-multi', entry.module_name)
    self.assertEqual('1.0', entry.min_version)
    self.assertEqual('null', entry.max_version)
    self.assertEqual('1.0', entry.resolved)
    self.assertEqual(2, len(entry.dependencies))
    self.assertTrue('p-bantam1' in entry.dependencies)
    self.assertTrue('p-bantam11' in entry.dependencies)

  # Test the method to serialize a LockEntry to a string
  def test_serialize(self):
    r = MockRegistry(REGISTRY_PATH)

    entry = LockEntry.from_module(r.module('bantam'))
    self.assertEqual('bantam:\n  min_version: 1.0\n  max_version: null\n  resolved: 2.1', entry.serialize())

    entry = LockEntry.from_module(r.module('p-bantam1'))
    self.assertEqual('p-bantam1:\n  min_version: 1.0\n  max_version: null\n  resolved: 1.0\n  dependencies:\n    bantam', entry.serialize())

    entry = LockEntry.from_module(r.module('p-multi'))
    self.assertEqual('p-multi:\n  min_version: 1.0\n  max_version: null\n  resolved: 1.0\n  dependencies:\n    p-bantam11\n    p-bantam1', entry.serialize())

  # Test the method to mark a LockEntry and all its dependencies as prefix
  def test_mark_prefix_recursive(self):
    r = MockRegistry(REGISTRY_PATH)

    entries = {
      'bantam': LockEntry.from_module(r.module('bantam')),
      'p-bantam1': LockEntry.from_module(r.module('p-bantam1')),
      'p-bantam11': LockEntry.from_module(r.module('p-bantam11')),
      'p-chained': LockEntry.from_module(r.module('p-chained'))
    }

    self.assertFalse(entries['bantam'].prefix)
    self.assertFalse(entries['p-bantam1'].prefix)
    self.assertFalse(entries['p-bantam11'].prefix)
    self.assertTrue(entries['p-chained'].prefix)

    entries['p-chained'].mark_prefix_recursive(entries)

    self.assertTrue(entries['bantam'].prefix)
    self.assertFalse(entries['p-bantam1'].prefix)
    self.assertTrue(entries['p-bantam11'].prefix)
    self.assertTrue(entries['p-chained'].prefix)


# A class to test the Lockfile object
class TestLockfile(unittest.TestCase):

  # Test the method to see if this Lockfile already contains a module
  def test_contains(self):
    r = MockRegistry(REGISTRY_PATH)

    lf = Lockfile()
    self.assertEqual(0, len(lf.prefix))
    self.assertEqual(0, len(lf.list))

    self.assertFalse(lf.contains('bantam'))
    self.assertFalse(lf.contains('p-bantam1'))
    self.assertFalse(lf.contains('p-bantam11'))
    self.assertFalse(lf.contains('p-multi'))

    lf.prefix.append(LockEntry.from_module(r.module('bantam')))
    self.assertTrue(lf.contains('bantam'))
    self.assertFalse(lf.contains('p-bantam1'))
    self.assertFalse(lf.contains('p-bantam11'))
    self.assertFalse(lf.contains('p-multi'))

    lf.list.append(LockEntry.from_module(r.module('p-bantam1')))
    self.assertTrue(lf.contains('bantam'))
    self.assertTrue(lf.contains('p-bantam1'))
    self.assertFalse(lf.contains('p-bantam11'))
    self.assertFalse(lf.contains('p-multi'))

    lf.list.append(LockEntry.from_module(r.module('p-bantam11')))
    self.assertTrue(lf.contains('bantam'))
    self.assertTrue(lf.contains('p-bantam1'))
    self.assertTrue(lf.contains('p-bantam11'))
    self.assertFalse(lf.contains('p-multi'))

    lf.list.append(LockEntry.from_module(r.module('p-multi')))
    self.assertTrue(lf.contains('bantam'))
    self.assertTrue(lf.contains('p-bantam1'))
    self.assertTrue(lf.contains('p-bantam11'))
    self.assertTrue(lf.contains('p-multi'))

  # Test the method to see if this LockFile meets the dependency requirements for a module
  def test_has_prereqs(self):
    r = MockRegistry(REGISTRY_PATH)

    lf = Lockfile()
    self.assertEqual(0, len(lf.prefix))
    self.assertEqual(0, len(lf.list))

    entries = {
      'bantam': LockEntry.from_module(r.module('bantam')),
      'p-bantam1': LockEntry.from_module(r.module('p-bantam1')),
      'p-bantam11': LockEntry.from_module(r.module('p-bantam11')),
      'p-multi': LockEntry.from_module(r.module('p-multi'))
    }

    self.assertTrue(lf.has_prereqs(entries['bantam']))
    self.assertFalse(lf.has_prereqs(entries['p-bantam1']))
    self.assertFalse(lf.has_prereqs(entries['p-bantam11']))
    self.assertFalse(lf.has_prereqs(entries['p-multi']))

    lf.prefix.append(entries['bantam'])
    self.assertTrue(lf.has_prereqs(entries['bantam']))
    self.assertTrue(lf.has_prereqs(entries['p-bantam1']))
    self.assertTrue(lf.has_prereqs(entries['p-bantam11']))
    self.assertFalse(lf.has_prereqs(entries['p-multi']))

    lf.list.append(entries['p-bantam1'])
    self.assertTrue(lf.has_prereqs(entries['bantam']))
    self.assertTrue(lf.has_prereqs(entries['p-bantam1']))
    self.assertTrue(lf.has_prereqs(entries['p-bantam11']))
    self.assertFalse(lf.has_prereqs(entries['p-multi']))

    lf.list.append(entries['p-bantam11'])
    self.assertTrue(lf.has_prereqs(entries['bantam']))
    self.assertTrue(lf.has_prereqs(entries['p-bantam1']))
    self.assertTrue(lf.has_prereqs(entries['p-bantam11']))
    self.assertTrue(lf.has_prereqs(entries['p-multi']))

  # Test the method to add a new module to this Lockfile
  def test_add(self):
    r = MockRegistry(REGISTRY_PATH)

    lf = Lockfile()
    self.assertEqual(0, len(lf.prefix))
    self.assertEqual(0, len(lf.list))

    entries = {
      'bantam': LockEntry.from_module(r.module('bantam')),
      'p-bantam1': LockEntry.from_module(r.module('p-bantam1')),
      'p-bantam11': LockEntry.from_module(r.module('p-bantam11')),
      'p-multi': LockEntry.from_module(r.module('p-multi'))
    }
    entries['bantam'].prefix = True

    with self.assertRaises(ValueError):
      lf.add(entries['p-multi'])

    lf.add(entries['bantam'])
    self.assertEqual(1, len(lf.prefix))
    self.assertEqual('bantam', lf.prefix[0].module_name)
    self.assertEqual(0, len(lf.list))

    lf.add(entries['p-bantam1'])
    lf.add(entries['p-bantam11'])
    self.assertEqual(1, len(lf.prefix))
    self.assertEqual(2, len(lf.list))
    self.assertEqual('p-bantam1', lf.list[0].module_name)
    self.assertEqual('p-bantam11', lf.list[1].module_name)

    lf.add(entries['p-multi'])
    self.assertEqual(1, len(lf.prefix))
    self.assertEqual(3, len(lf.list))
    self.assertEqual('p-multi', lf.list[2].module_name)

    lf.add(entries['p-multi'])
    self.assertEqual(1, len(lf.prefix))
    self.assertEqual(3, len(lf.list))

  # Test the method to serialize the Lockfile to a string
  def test_serialize(self):
    r = MockRegistry(REGISTRY_PATH)
    lf = Lockfile()

    entries = {
      'bantam': LockEntry.from_module(r.module('bantam')),
      'p-bantam1': LockEntry.from_module(r.module('p-bantam1')),
      'p-bantam11': LockEntry.from_module(r.module('p-bantam11')),
      'p-chained': LockEntry.from_module(r.module('p-chained')),
      'p-multi': LockEntry.from_module(r.module('p-multi'))
    }

    lf.add(entries['bantam'])
    lf.add(entries['p-bantam11'])

    self.assertEqual('# THIS FILE IS GENERATED AUTOMATICALLY. DO NOT MODIFY DIRECTLY.\n\nbantam:\n  min_version: 1.0\n  max_version: null\n  resolved: 2.1\n\np-bantam11:\n  min_version: 1.0\n  max_version: null\n  resolved: 1.0\n  dependencies:\n    bantam', lf.serialize())

    lf = Lockfile()

    entries['bantam'].prefix = True
    entries['p-bantam11'].prefix = True
    entries['p-chained'].prefix = True

    lf.add(entries['bantam'])
    lf.add(entries['p-bantam11'])
    lf.add(entries['p-chained'])
    lf.add(entries['p-bantam1'])
    lf.add(entries['p-multi'])

    self.assertEqual('# THIS FILE IS GENERATED AUTOMATICALLY. DO NOT MODIFY DIRECTLY.\n\nbantam:\n  min_version: 1.0\n  max_version: null\n  resolved: 2.1\n\np-bantam11:\n  min_version: 1.0\n  max_version: null\n  resolved: 1.0\n  dependencies:\n    bantam\n\np-chained:\n  min_version: 1.0\n  max_version: null\n  resolved: 1.0\n  dependencies:\n    p-bantam11\n\n--------------------------------------------------\n\np-bantam1:\n  min_version: 1.0\n  max_version: null\n  resolved: 1.0\n  dependencies:\n    bantam\n\np-multi:\n  min_version: 1.0\n  max_version: null\n  resolved: 1.0\n  dependencies:\n    p-bantam11\n    p-bantam1', lf.serialize())

  # Test the method to write this Lockfile to a file on disk
  def test_write(self):
    lf = Lockfile()
    self.assertFalse(IOOperations.file_exists(LOCK_PATH))

    lf.write(LOCK_PATH)
    self.assertTrue(IOOperations.file_exists(LOCK_PATH))

    os.remove(LOCK_PATH)
    self.assertFalse(IOOperations.file_exists(LOCK_PATH))

  # Test the method to load a Lockfile object from a string
  def test_loads(self):
    representation = '# THIS FILE IS GENERATED AUTOMATICALLY. DO NOT MODIFY DIRECTLY.\n\nbantam:\n  min_version: 1.0\n  max_version: null\n  resolved: 2.1\n\np-bantam11:\n  min_version: 1.0\n  max_version: 1.0.0\n  resolved: 1.0\n  dependencies:\n    bantam\n\np-chained:\n  min_version: 1.0\n  max_version: 1.0.0\n  resolved: 1.0\n  dependencies:\n    p-bantam11\n\n--------------------------------------------------\n\np-bantam1:\n  min_version: 1.0\n  max_version: 1.0.0\n  resolved: 1.0\n  dependencies:\n    bantam\n\np-multi:\n  min_version: 1.0\n  max_version: 1.0.0\n  resolved: 1.0\n  dependencies:\n    p-bantam11\n    p-bantam1'

    lf = Lockfile.loads(representation)

    self.assertEqual('Lockfile', lf.__class__.__name__)
    self.assertEqual(3, len(lf.prefix))
    self.assertEqual(2, len(lf.list))

    self.assertEqual('bantam', lf.prefix[0].module_name)
    self.assertTrue(lf.prefix[0].prefix)
    self.assertEqual('1.0', lf.prefix[0].min_version)
    self.assertEqual('null', lf.prefix[0].max_version)
    self.assertEqual('2.1', lf.prefix[0].resolved)
    self.assertEqual(0, len(lf.prefix[0].dependencies))

    self.assertEqual('p-bantam11', lf.prefix[1].module_name)
    self.assertTrue(lf.prefix[1].prefix)
    self.assertEqual('1.0', lf.prefix[1].min_version)
    self.assertEqual('1.0.0', lf.prefix[1].max_version)
    self.assertEqual('1.0', lf.prefix[1].resolved)
    self.assertEqual(1, len(lf.prefix[1].dependencies))
    self.assertEqual('bantam', lf.prefix[1].dependencies[0])

    self.assertEqual('p-chained', lf.prefix[2].module_name)
    self.assertTrue(lf.prefix[2].prefix)
    self.assertEqual('1.0', lf.prefix[2].min_version)
    self.assertEqual('1.0.0', lf.prefix[2].max_version)
    self.assertEqual('1.0', lf.prefix[2].resolved)
    self.assertEqual(1, len(lf.prefix[2].dependencies))
    self.assertEqual('p-bantam11', lf.prefix[2].dependencies[0])

    self.assertEqual('p-bantam1', lf.list[0].module_name)
    self.assertFalse(lf.list[0].prefix)
    self.assertEqual('1.0', lf.list[0].min_version)
    self.assertEqual('1.0.0', lf.list[0].max_version)
    self.assertEqual('1.0', lf.list[0].resolved)
    self.assertEqual(1, len(lf.list[0].dependencies))
    self.assertEqual('bantam', lf.list[0].dependencies[0])

    self.assertEqual('p-multi', lf.list[1].module_name)
    self.assertFalse(lf.list[1].prefix)
    self.assertEqual('1.0', lf.list[1].min_version)
    self.assertEqual('1.0.0', lf.list[1].max_version)
    self.assertEqual('1.0', lf.list[1].resolved)
    self.assertEqual(2, len(lf.list[1].dependencies))
    self.assertEqual('p-bantam11', lf.list[1].dependencies[0])
    self.assertEqual('p-bantam1', lf.list[1].dependencies[1])

    self.assertEqual(representation, lf.serialize())

  # Test the method to load a Lockfile object from a file on disk
  def test_loadf(self):
    representation = '# THIS FILE IS GENERATED AUTOMATICALLY. DO NOT MODIFY DIRECTLY.\n\nbantam:\n  min_version: 1.0\n  max_version: null\n  resolved: 2.1\n\np-bantam11:\n  min_version: 1.0\n  max_version: 1.0.0\n  resolved: 1.0\n  dependencies:\n    bantam\n\np-chained:\n  min_version: 1.0\n  max_version: 1.0.0\n  resolved: 1.0\n  dependencies:\n    p-bantam11\n\n--------------------------------------------------\n\np-bantam1:\n  min_version: 1.0\n  max_version: 1.0.0\n  resolved: 1.0\n  dependencies:\n    bantam\n\np-multi:\n  min_version: 1.0\n  max_version: 1.0.0\n  resolved: 1.0\n  dependencies:\n    p-bantam11\n    p-bantam1'
    with open(LOCK_PATH, 'w') as f:
      f.write(representation)

    self.assertTrue(IOOperations.file_exists(LOCK_PATH))

    lf = Lockfile.loadf(LOCK_PATH)

    self.assertEqual('Lockfile', lf.__class__.__name__)
    self.assertEqual(3, len(lf.prefix))
    self.assertEqual(2, len(lf.list))

    self.assertEqual('bantam', lf.prefix[0].module_name)
    self.assertTrue(lf.prefix[0].prefix)
    self.assertEqual('1.0', lf.prefix[0].min_version)
    self.assertEqual('null', lf.prefix[0].max_version)
    self.assertEqual('2.1', lf.prefix[0].resolved)
    self.assertEqual(0, len(lf.prefix[0].dependencies))

    self.assertEqual('p-bantam11', lf.prefix[1].module_name)
    self.assertTrue(lf.prefix[1].prefix)
    self.assertEqual('1.0', lf.prefix[1].min_version)
    self.assertEqual('1.0.0', lf.prefix[1].max_version)
    self.assertEqual('1.0', lf.prefix[1].resolved)
    self.assertEqual(1, len(lf.prefix[1].dependencies))
    self.assertEqual('bantam', lf.prefix[1].dependencies[0])

    self.assertEqual('p-chained', lf.prefix[2].module_name)
    self.assertTrue(lf.prefix[2].prefix)
    self.assertEqual('1.0', lf.prefix[2].min_version)
    self.assertEqual('1.0.0', lf.prefix[2].max_version)
    self.assertEqual('1.0', lf.prefix[2].resolved)
    self.assertEqual(1, len(lf.prefix[2].dependencies))
    self.assertEqual('p-bantam11', lf.prefix[2].dependencies[0])

    self.assertEqual('p-bantam1', lf.list[0].module_name)
    self.assertFalse(lf.list[0].prefix)
    self.assertEqual('1.0', lf.list[0].min_version)
    self.assertEqual('1.0.0', lf.list[0].max_version)
    self.assertEqual('1.0', lf.list[0].resolved)
    self.assertEqual(1, len(lf.list[0].dependencies))
    self.assertEqual('bantam', lf.list[0].dependencies[0])

    self.assertEqual('p-multi', lf.list[1].module_name)
    self.assertFalse(lf.list[1].prefix)
    self.assertEqual('1.0', lf.list[1].min_version)
    self.assertEqual('1.0.0', lf.list[1].max_version)
    self.assertEqual('1.0', lf.list[1].resolved)
    self.assertEqual(2, len(lf.list[1].dependencies))
    self.assertEqual('p-bantam11', lf.list[1].dependencies[0])
    self.assertEqual('p-bantam1', lf.list[1].dependencies[1])

    self.assertEqual(representation, lf.serialize())

    os.remove(LOCK_PATH)
    self.assertFalse(IOOperations.file_exists(LOCK_PATH))


# A class to test the manifest Resolver helper class
class TestResolver(unittest.TestCase):

  # Test the initialization of a new Resolver object
  def test_init(self):
    m = Manifest()
    r = MockRegistry(REGISTRY_PATH)

    resolver = Resolver(m, r)

    self.assertEqual(m, resolver.manifest)
    self.assertEqual(r, resolver.registry)
    self.assertEqual(None, resolver.staging)
    self.assertEqual(None, resolver.lock_staging)

  # Test the method for Resolving module dependencies in the basic case
  def test_resolve_basic(self):
    m = Manifest()
    m.dependencies.append(Dependency('p-bantam11', '+1.0'))

    r = MockRegistry(REGISTRY_PATH)

    resolver = Resolver(m, r)

    lf = resolver.resolve()

    self.assertEqual(0, len(lf.prefix))

    self.assertEqual(2, len(lf.list))
    self.assertEqual('bantam', lf.list[0].module_name)
    self.assertEqual('2.1', lf.list[0].resolved)
    self.assertEqual('p-bantam11', lf.list[1].module_name)
    self.assertEqual('1.0', lf.list[1].resolved)

  # Test the method to resolve module dependencies with prefix modules
  def test_resolve_prefix(self):
    m = Manifest()
    m.dependencies.append(Dependency('p-multi', '+1.0'))
    m.dependencies.append(Dependency('p-chained', '+1.0'))

    r = MockRegistry(REGISTRY_PATH)

    resolver = Resolver(m, r)

    lf = resolver.resolve()

    self.assertEqual(3, len(lf.prefix))
    self.assertEqual('bantam', lf.prefix[0].module_name)
    self.assertEqual('1.2', lf.prefix[0].resolved)
    self.assertEqual('p-bantam11', lf.prefix[1].module_name)
    self.assertEqual('1.0', lf.prefix[1].resolved)
    self.assertEqual('p-chained', lf.prefix[2].module_name)
    self.assertEqual('1.0', lf.prefix[2].resolved)

    self.assertEqual(2, len(lf.list))
    self.assertEqual('p-bantam1', lf.list[0].module_name)
    self.assertEqual('1.0', lf.list[0].resolved)
    self.assertEqual('p-multi', lf.list[1].module_name)
    self.assertEqual('1.0', lf.list[1].resolved)

  # Test the method to resolve module dependencies with a circular reference
  def test_resolve_circular(self):
    m = Manifest()
    m.dependencies.append(Dependency('p-circular', '+1.0'))

    r = MockRegistry(REGISTRY_PATH)

    resolver = Resolver(m, r)

    with self.assertRaises(OverflowError):
      lf = resolver.resolve()


# A class to test the script manipulation utils
class TestScripts(unittest.TestCase):

  # Test the Javascript minification method
  def test_minify_js(self):
    self.assertEqual('let a=1; let b=2; let c=3;',
      Scripts.minify_js('\n\t  let  a=1;  \n\t let b=2; \n\tlet c=3;  \n\n\n'))

  # Test the CSS minification
  def test_minify_css(self):
    self.assertEqual('body {font-family: sans-serif;font-size: 14pt;}',
      Scripts.minify_css('\n  body {\n\tfont-family:  sans-serif;\n\tfont-size:      14pt;\n}   \n\n\n'))

  # Test the method for compiling module scripts into their respective files
  def test_build_module(self):
    manifest = Manifest()

    mdl = LockEntry()
    mdl.module_name = 'p-multi'
    mdl.min_version = '1.0'
    mdl.max_version = '1.0.0'
    mdl.resolved = '1.0'
    mdl.dependencies = ['p-bantam1', 'p-bantam11']

    self.assertFalse(IOOperations.directory_exists(manifest.module_directory))

    IOOperations.mkdir(manifest.module_directory)
    IOOperations.mkdir(join(manifest.module_directory, mdl.module_name))
    path = join(manifest.module_directory, mdl.module_name, mdl.resolved)
    IOOperations.mkdir(path)

    self.assertTrue(IOOperations.directory_exists(path))

    copyfile(join('testing','p-multi.js'), join(path,'p-multi.js'))
    copyfile(join('testing','p-multi.css'), join(path,'p-multi.css'))

    self.assertTrue(IOOperations.file_exists(join(path,'p-multi.js')))
    self.assertTrue(IOOperations.file_exists(join(path,'p-multi.css')))

    results = Scripts.build_module(manifest, mdl)

    self.assertEqual(list, type(results))
    self.assertEqual(3, len(results))
    self.assertEqual('let mdl = \'p-multi\';', results[0])
    self.assertEqual('body { font-family: sans-serif; font-size: 14pt;}', results[1])
    self.assertEqual('',results[2])

    mdl.prefix = True

    results = Scripts.build_module(manifest, mdl)

    self.assertEqual(list, type(results))
    self.assertEqual(3, len(results))
    self.assertEqual('',results[0])
    self.assertEqual('body { font-family: sans-serif; font-size: 14pt;}', results[1])
    self.assertEqual('let mdl = \'p-multi\';', results[2])

    IOOperations.rmdir(manifest.module_directory, recursive=True)

    self.assertFalse(IOOperations.directory_exists(manifest.module_directory))


# A class to test the actual command methods within this utility
class TestToolCommands(unittest.TestCase):

  # Test the init command
  def test_init_command(self):
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))
    init([])
    self.assertTrue(IOOperations.file_exists(MANIFEST_PATH))

    m = Manifest.loadf(MANIFEST_PATH)

    self.assertEqual('bantam_modules', m.module_directory)
    self.assertEqual(join('scripts','dependencies.js'), m.js_target)
    self.assertEqual(join('styles','dependencies.css'), m.css_target)
    self.assertEqual(join('scripts','prefix-dependencies.js'), m.prefix_target)
    self.assertEqual(0, len(m.dependencies))

    os.remove(MANIFEST_PATH)
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))

  # Test the module add command
  def test_add_command(self):
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))
    init([])
    self.assertTrue(IOOperations.file_exists(MANIFEST_PATH))

    m = Manifest.loadf(MANIFEST_PATH)
    self.assertEqual(0, len(m.dependencies))

    add('bantam','^2.1',[])
    add('bantam-tabs','+1.0', [])
    add('bantam-toc','1.2', [])

    m = Manifest.loadf(MANIFEST_PATH)
    self.assertEqual(3, len(m.dependencies))

    d = [d for d in m.dependencies if d.module_name == 'bantam'][0]
    self.assertEqual('bantam', d.module_name)
    self.assertEqual('^2.1', d.version_string)
    d = [d for d in m.dependencies if d.module_name == 'bantam-tabs'][0]
    self.assertEqual('bantam-tabs', d.module_name)
    self.assertEqual('+1.0', d.version_string)
    d = [d for d in m.dependencies if d.module_name == 'bantam-toc'][0]
    self.assertEqual('bantam-toc', d.module_name)
    self.assertEqual('1.2', d.version_string)

    os.remove(MANIFEST_PATH)
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))

  # Test the module remove command
  def test_rm_command(self):
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))
    init([])
    self.assertTrue(IOOperations.file_exists(MANIFEST_PATH))

    add('bantam','^2.1',[])
    add('bantam-tabs','+1.0', [])
    add('bantam-toc','1.2', [])

    m = Manifest.loadf(MANIFEST_PATH)
    self.assertEqual(3, len(m.dependencies))

    d = [d for d in m.dependencies if d.module_name == 'bantam'][0]
    self.assertEqual('bantam', d.module_name)
    self.assertEqual('^2.1', d.version_string)
    d = [d for d in m.dependencies if d.module_name == 'bantam-tabs'][0]
    self.assertEqual('bantam-tabs', d.module_name)
    self.assertEqual('+1.0', d.version_string)
    d = [d for d in m.dependencies if d.module_name == 'bantam-toc'][0]
    self.assertEqual('bantam-toc', d.module_name)
    self.assertEqual('1.2', d.version_string)

    IOOperations.mkdir(m.module_directory)
    IOOperations.mkdir(join(m.module_directory, 'bantam'))
    IOOperations.mkdir(join(m.module_directory, 'bantam', '2.1'))
    IOOperations.mkdir(join(m.module_directory, 'bantam-tabs'))
    IOOperations.mkdir(join(m.module_directory, 'bantam-tabs', '1.1.1'))
    IOOperations.mkdir(join(m.module_directory, 'bantam-toc'))
    IOOperations.mkdir(join(m.module_directory, 'bantam-toc', '1.2'))
    self.assertTrue(IOOperations.directory_exists(join(m.module_directory, 'bantam', '2.1')))
    self.assertTrue(IOOperations.directory_exists(join(m.module_directory, 'bantam-tabs', '1.1.1')))
    self.assertTrue(IOOperations.directory_exists(join(m.module_directory, 'bantam-toc', '1.2')))

    rm('bantam-toc',[])

    m = Manifest.loadf(MANIFEST_PATH)
    self.assertEqual(2, len(m.dependencies))

    d = [d for d in m.dependencies if d.module_name == 'bantam'][0]
    self.assertEqual('bantam', d.module_name)
    self.assertEqual('^2.1', d.version_string)
    d = [d for d in m.dependencies if d.module_name == 'bantam-tabs'][0]
    self.assertEqual('bantam-tabs', d.module_name)
    self.assertEqual('+1.0', d.version_string)
    self.assertTrue(IOOperations.directory_exists(join(m.module_directory, 'bantam', '2.1')))
    self.assertTrue(IOOperations.directory_exists(join(m.module_directory, 'bantam-tabs', '1.1.1')))
    self.assertTrue(IOOperations.directory_exists(join(m.module_directory, 'bantam-toc', '1.2')))

    rm('bantam-tabs',['-f'])

    m = Manifest.loadf(MANIFEST_PATH)
    self.assertEqual(1, len(m.dependencies))

    d = [d for d in m.dependencies if d.module_name == 'bantam'][0]
    self.assertEqual('bantam', d.module_name)
    self.assertEqual('^2.1', d.version_string)
    self.assertTrue(IOOperations.directory_exists(join(m.module_directory, 'bantam', '2.1')))
    self.assertFalse(IOOperations.directory_exists(join(m.module_directory, 'bantam-tabs', '1.1.1')))
    self.assertTrue(IOOperations.directory_exists(join(m.module_directory, 'bantam-toc', '1.2')))

    os.remove(MANIFEST_PATH)
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))
    IOOperations.rmdir(m.module_directory, recursive=True)
    self.assertFalse(IOOperations.directory_exists(m.module_directory))

  # Test the resolve command to resolve dependencies and generate the lockfile
  def test_resolve_command(self):
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))
    self.assertFalse(IOOperations.file_exists(LOCK_PATH))

    init([])

    self.assertTrue(IOOperations.file_exists(MANIFEST_PATH))

    add('p-chained','1.0',[])
    add('p-multi','1.0',[])

    r = MockRegistry(REGISTRY_PATH)

    resolve(['-q'], registry=r)

    self.assertTrue(IOOperations.file_exists(LOCK_PATH))
    with open(LOCK_PATH, 'r') as f:
      data = f.read()

    self.assertEqual('# THIS FILE IS GENERATED AUTOMATICALLY. DO NOT MODIFY DIRECTLY.\n\nbantam:\n  min_version: 1.1\n  max_version: 2\n  resolved: 1.2\n\np-bantam11:\n  min_version: 1.0\n  max_version: 1.0.0\n  resolved: 1.0\n  dependencies:\n    bantam\n\np-chained:\n  min_version: 1.0\n  max_version: 1.0.0\n  resolved: 1.0\n  dependencies:\n    p-bantam11\n\n--------------------------------------------------\n\np-bantam1:\n  min_version: 1.0\n  max_version: 1.0.0\n  resolved: 1.0\n  dependencies:\n    bantam\n\np-multi:\n  min_version: 1.0\n  max_version: 1.0.0\n  resolved: 1.0\n  dependencies:\n    p-bantam11\n    p-bantam1', data)

    os.remove(MANIFEST_PATH)
    os.remove(LOCK_PATH)
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))
    self.assertFalse(IOOperations.file_exists(LOCK_PATH))

  # Test the install command to install local module files
  def test_install(self):
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))
    self.assertFalse(IOOperations.file_exists(LOCK_PATH))

    init([])
    self.assertTrue(IOOperations.file_exists(MANIFEST_PATH))

    add('p-bantam1', '1.0', [])
    add('p-bantam11', '1.0', [])

    m = Manifest.loadf(MANIFEST_PATH)
    self.assertFalse(IOOperations.directory_exists(m.module_directory))

    r = MockRegistry(REGISTRY_PATH)

    resolve(['-q'], registry=r)
    self.assertTrue(IOOperations.file_exists(LOCK_PATH))

    install([], registry=r)

    self.assertTrue(IOOperations.file_exists(join(m.module_directory,
      'bantam','1.2','bantam-1.2.min.js')))
    self.assertTrue(IOOperations.file_exists(join(m.module_directory,
      'bantam','1.2','LICENSE.txt')))
    self.assertTrue(IOOperations.file_exists(join(m.module_directory,
      'p-bantam1','1.0','p-bantam1.js')))
    self.assertTrue(IOOperations.file_exists(join(m.module_directory,
      'p-bantam11','1.0','p-bantam11.js')))

    IOOperations.rmdir(m.module_directory, recursive=True)
    self.assertFalse(IOOperations.directory_exists(m.module_directory))

    os.remove(MANIFEST_PATH)
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))
    os.remove(LOCK_PATH)
    self.assertFalse(IOOperations.file_exists(LOCK_PATH))

  # Test the build command to compile modules into their respective files
  def test_build(self):
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))
    self.assertFalse(IOOperations.file_exists(LOCK_PATH))

    init([])
    self.assertTrue(IOOperations.file_exists(MANIFEST_PATH))

    add('p-multi', '1.0', [])
    add('p-chained', '1.0', [])

    m = Manifest.loadf(MANIFEST_PATH)
    m.js_target = join('testing','dependencies.js')
    m.css_target = join('testing','dependencies.css')
    m.prefix_target = join('testing','prefix-dependencies.js')
    m.write(MANIFEST_PATH)

    self.assertFalse(IOOperations.directory_exists(m.module_directory))

    r = MockRegistry(REGISTRY_PATH)

    resolve(['-q'], registry=r)
    self.assertTrue(IOOperations.file_exists(LOCK_PATH))

    install([], registry=r)

    self.assertFalse(IOOperations.file_exists(m.js_target))
    self.assertFalse(IOOperations.file_exists(m.css_target))
    self.assertFalse(IOOperations.file_exists(m.prefix_target))

    build([])

    self.assertTrue(IOOperations.file_exists(m.js_target))
    self.assertTrue(IOOperations.file_exists(m.css_target))
    self.assertTrue(IOOperations.file_exists(m.prefix_target))

    with open(m.prefix_target, 'r') as f:
      self.assertEqual('let mdl = \'bantam\'; let mdl = \'p-bantam11\'; let mdl = \'p-chained\';', f.read())
    with open(m.css_target, 'r') as f:
      self.assertEqual('body { font-family: sans-serif; font-size: 14pt;}', f.read())
    with open(m.js_target, 'r') as f:
      self.assertEqual('let mdl = \'p-bantam1\'; let mdl = \'p-multi\';', f.read())

    os.remove(m.js_target)
    os.remove(m.css_target)
    os.remove(m.prefix_target)
    self.assertFalse(IOOperations.file_exists(m.js_target))
    self.assertFalse(IOOperations.file_exists(m.css_target))
    self.assertFalse(IOOperations.file_exists(m.prefix_target))

    IOOperations.rmdir(m.module_directory, recursive=True)
    self.assertFalse(IOOperations.directory_exists(m.module_directory))

    os.remove(MANIFEST_PATH)
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))
    os.remove(LOCK_PATH)
    self.assertFalse(IOOperations.file_exists(LOCK_PATH))

  # Test the run command to run the full pipeline
  def test_run(self):
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))
    self.assertFalse(IOOperations.file_exists(LOCK_PATH))

    init([])
    self.assertTrue(IOOperations.file_exists(MANIFEST_PATH))

    add('p-multi', '1.0', [])
    add('p-chained', '1.0', [])

    m = Manifest.loadf(MANIFEST_PATH)
    m.js_target = join('testing','dependencies.js')
    m.css_target = join('testing','dependencies.css')
    m.prefix_target = join('testing','prefix-dependencies.js')
    m.write(MANIFEST_PATH)

    self.assertFalse(IOOperations.directory_exists(m.module_directory))

    r = MockRegistry(REGISTRY_PATH)

    run(['-q'], registry=r)

    self.assertTrue(IOOperations.file_exists(LOCK_PATH))
    self.assertTrue(IOOperations.file_exists(m.js_target))
    self.assertTrue(IOOperations.file_exists(m.css_target))
    self.assertTrue(IOOperations.file_exists(m.prefix_target))

    with open(m.prefix_target, 'r') as f:
      self.assertEqual('let mdl = \'bantam\'; let mdl = \'p-bantam11\'; let mdl = \'p-chained\';', f.read())
    with open(m.css_target, 'r') as f:
      self.assertEqual('body { font-family: sans-serif; font-size: 14pt;}', f.read())
    with open(m.js_target, 'r') as f:
      self.assertEqual('let mdl = \'p-bantam1\'; let mdl = \'p-multi\';', f.read())

    os.remove(m.js_target)
    os.remove(m.css_target)
    os.remove(m.prefix_target)
    self.assertFalse(IOOperations.file_exists(m.js_target))
    self.assertFalse(IOOperations.file_exists(m.css_target))
    self.assertFalse(IOOperations.file_exists(m.prefix_target))

    IOOperations.rmdir(m.module_directory, recursive=True)
    self.assertFalse(IOOperations.directory_exists(m.module_directory))

    os.remove(MANIFEST_PATH)
    self.assertFalse(IOOperations.file_exists(MANIFEST_PATH))
    os.remove(LOCK_PATH)
    self.assertFalse(IOOperations.file_exists(LOCK_PATH))



# Actually execute the unit tests
if __name__ == '__main__':
    unittest.main()
