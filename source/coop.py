# The Bantam Coop module manager
# version 1.3
# created by Caspar Anderegg

# ------------------------------------------------------------------------
# CONFIGURATION CONSTANTS

# The version string of this tool
VERSION = '1.3'

# The base URL of the Coop module registry
REGISTRY_PATH = 'https://canderegg.gitlab.io/bantam-coop/'

# The path of the manifest file
MANIFEST_PATH = 'manifest.json'

# The path of the lock file
LOCK_PATH = 'coop.lock'

# The maximum number of times to loop when generating the Lockfile
MAX_LOOP_COUNT = 100

# ------------------------------------------------------------------------
# REQUIRED PACKAGE IMPORTS

import sys
import os
import shutil
import json

# Set up environment based on Python version
if sys.version_info[0] < 3:
  import urllib2

  FileNotFoundError = IOError
else:
  import urllib.request
  import urllib.error

# ------------------------------------------------------------------------
# HELPER CLASSES

# A class to house a specific dependency
class Dependency:
  # Initialize a new dependency with the given module and version
  def __init__(self, module, version):
    self.module_name = module.lower()
    self.version_string = version

  # Get the minimum eligible version of this dependency
  def get_min_version(self):
    try:
      return Dependency.parse_version(self.version_string
        .replace('^','').replace('+',''))
    except ValueError:
        raise ValueError('Non-numeric version string passed.')

  # Get the maximum eligible version of this dependency
  def get_max_version(self):
    min_version = self.get_min_version()
    # If no upper bound, return none
    if '+' in self.version_string:
      return None
    # If limit is next major version, return that
    elif '^' in self.version_string:
      return [min_version[0] + 1]
    # Otherwise limit is next patch version, return that
    else:
      min_version.append(0)
      return min_version

  # How to convert this dependency to a string
  def __str__(self):
    return self.module_name + '@' + self.version_string

  # The human readable representation of this dependency
  def __repr__(self):
    return str(self)

  # Serialize this dependency into JSON
  def serialize(self):
    return '"' + self.module_name + '": "' + self.version_string + '"'

  # Parse the version information from the given version string
  @staticmethod
  def parse_version(info):
    if info.lower() == 'null':
      return None

    if '+' in info or '-' in info or '^' in info:
      raise ValueError('Version range passed.')

    try:
      return [int(v) for v in info.split('.')]
    except ValueError:
      raise ValueError('Non-numeric version string passed.')

  # Turn a numeric version into a version string
  @staticmethod
  def version_string(info):
    if info is None or len(info) < 1:
      return 'null'

    return '.'.join([str(n) for n in info])

  # Compare a set of version to determine which is greater
  #   returns <0 if v1 < v2, 0 if v1 == v2, and >0 if v1 > v2
  @staticmethod
  def compare(v1, v2):
    # Check if either is None
    if v1 is None and v2 is None:
      return 0
    if v2 is None:
      return -1
    elif v1 is None:
      return 1

    # Loop over the version digits
    working = 0
    while working < len(v1) and working < len(v2):
      if v1[working] != v2[working]:
        return v1[working] - v2[working]
      working+= 1

    # At this point we know one version is a prefix of the other
    return len(v1) - len(v2)


# A class to manage the project manifest file
class Manifest:
  # Initialize a new empty manifest
  def __init__(self):
    self.module_directory = 'bantam_modules'
    self.js_target = os.path.join('scripts','dependencies.js')
    self.css_target = os.path.join('styles','dependencies.css')
    self.prefix_target = os.path.join('scripts','prefix-dependencies.js')
    self.dependencies = []

  # Serialize this manifest into JSON
  def serialize(self):
    obj = {'module_directory': self.module_directory,
      'js_target': self.js_target,
      'css_target': self.css_target,
      'prefix_target': self.prefix_target,
      'dependencies': {}}

    for d in self.dependencies:
      obj['dependencies'][d.module_name] = d.version_string

    return json.dumps(obj, sort_keys=True, indent=2)

  # Write this serialized manifest into a file
  def write(self, fpath):
    with open(fpath, 'w') as f:
      f.write(self.serialize())

  # Create a new Manifest object from the provided JSON string
  @staticmethod
  def loads(json_str):
    data = json.loads(json_str)
    m = Manifest()

    m.module_directory = data['module_directory']
    m.js_target = data['js_target']
    m.css_target = data['css_target']
    m.prefix_target = data['prefix_target']

    for mdl in data['dependencies']:
      m.dependencies.append(Dependency(str(mdl), str(data['dependencies'][mdl])))

    return m

  # Create a new Manifest object from the given file path
  @staticmethod
  def loadf(fpath):
    try:
      with open(fpath, 'r') as f:
        json_str = f.read()
        return Manifest.loads(json_str)
    except (IOError, FileNotFoundError) as e:
      raise IOError('Manifest file not found, try running the init command first')


# A class to handle IO operations
class IOOperations:
  # Check if the provided file exists
  @staticmethod
  def file_exists(path):
    return os.path.exists(path) and os.path.isfile(path)

  # Check if the provided directory exists
  @staticmethod
  def directory_exists(path):
    return os.path.exists(path) and os.path.isdir(path)

  # Create the given directory
  @staticmethod
  def mkdir(path):
    os.mkdir(path)

  # Remove the given directory
  @staticmethod
  def rmdir(path, recursive=False):
    if not recursive:
      os.rmdir(path)
    else:
      shutil.rmtree(path)

  # Read the data from the provided URL
  @staticmethod
  def readurl(url):
    if sys.version_info[0] < 3:
      # Use the Python 2 version
      try:
        response = urllib2.urlopen(url)
        data = response.read().decode('utf-8')
        response.close()
        return data
      except urllib2.HTTPError as e:
        raise IOError(e.message)
    # Use the Python 3 version
    else:
      try:
        with urllib.request.urlopen(url) as response:
          return response.read().decode('utf-8')
      except urllib.error.HTTPError as e:
        raise IOError(e.reason)

  # Get the list of files with the specified file type at the specified path
  @staticmethod
  def get_files_of_type(path, ext):
    return [f for f in os.listdir(path)
      if os.path.isfile(os.path.join(path, f)) and f.endswith(ext)]

# A class to manage the connection to a registry index
class Registry:
  # Instantiate the registry object at the given root path
  def __init__(self, root):
    self.root = root
    self.index = None

  # Get data from the provided registry pathway
  def get(self, path):
    try:
      return IOOperations.readurl(path)
    except IOError as e:
      print('ERROR: Unable to read pathway. ' + str(e))

  # Get the module index from the registry
  def module_index(self):
    path = self.root + 'modules.json'
    return json.loads(self.get(path))

  # Get the specific module entry from the registry
  def module(self, mdl):
    if self.index is None:
      self.index = self.module_index()

    if not mdl in self.index:
      raise ValueError('Unable to find the specified module.')

    entry_data = self.get(self.index[mdl])
    return RegistryEntry.loads(entry_data)


# A class to handle information for a specific module version
class Version:
  # Initialize a new version from the given metadata
  def __init__(self, key, files):
    try:
      self.version_string = key
      self.numeric_version = Dependency.parse_version(self.version_string)
      self.files = files
    except:
      raise ValueError('Unable to parse module version: ' + key)

  # Implement equality checking
  def __eq__(self, other):
    try:
      return Dependency.compare(self.numeric_version, other.numeric_version) == 0
    except:
      return False

  # Implement less than checking
  def __lt__(self, other):
    try:
      return Dependency.compare(self.numeric_version, other.numeric_version) < 0
    except:
      return False


# A class to represent the registry index for a specific module
class RegistryEntry:
  # Create a new registry index from the given JSON object
  def __init__(self, data):
    try:
      # Parse the metadata
      self.module_name = data['module-name']
      self.module_desc = data['module-desc']
      self.author = data['author']
      self.documentation = data['documentation']
      self.repository = data['repository']
      self.license = data['license']
      self.prefix = False
      if 'prefix' in data:
        self.prefix = data['prefix']

      # Add the dependencies
      self.dependencies = []
      for mdl in data['dependencies']:
        self.dependencies.append(Dependency(mdl, data['dependencies'][mdl]))

      # Add the eligible versions
      self.versions = []
      for v in data['versions']:
        self.versions.append(Version(v, data['versions'][v]))
      self.versions.sort()

      # Add the version constraints
      self.min_version = [0]
      if len(self.versions) > 0:
        self.min_version = self.versions[0].numeric_version
      self.max_version = None

    except:
      raise ValueError('Unable to parse registry index, module: ' + self.module_name)

  # Add a new constraint to this module
  def add_constraint(self, dependency):
    # If this is a dependency on a different module, do nothing
    if (self.module_name != dependency.module_name):
      return

    # Update the minimum version if required
    if Dependency.compare(self.min_version, dependency.get_min_version()) < 0:
      self.min_version = dependency.get_min_version()

    # Update the maximum version if required
    if Dependency.compare(dependency.get_max_version(), self.max_version) < 0:
      self.max_version = dependency.get_max_version()

  # Resolve this module to a specific candidate version
  # Returns the latest eligible version_string, or None if no eligible candidate
  def get_install_version(self):
    # Filter the list of eligible candidate versions
    candidates = [v for v in self.versions
      if (Dependency.compare(self.min_version, v.numeric_version) <= 0
      and Dependency.compare(v.numeric_version, self.max_version) < 0)]

    # If no eligible candidate versions, return None
    if len(candidates) < 1:
      return None

    # Otherwise return the version string
    return candidates[-1].version_string

  # Get the specific version information
  def get_version_files(self, version):
    vs = [v for v in self.versions if v.version_string == version]
    if len(vs) != 1:
      return []
    return vs[0].files

  # Parse a registry index out of the provided string
  @staticmethod
  def loads(data):
    return RegistryEntry(json.loads(data))


# A class to represent a single entry in the lock file
class LockEntry:
  # Create a new, blank lock entry
  def __init__(self):
    self.module_name = ''
    self.min_version = 'null'
    self.max_version = 'null'
    self.resolved = ''
    self.dependencies = []
    self.prefix = False

  # Serialize this lock entry
  def serialize(self):
    result = self.module_name + ':'
    result+= '\n  min_version: ' + self.min_version
    result+= '\n  max_version: ' + self.max_version
    result+= '\n  resolved: ' + self.resolved

    if len(self.dependencies) > 0:
      result+= '\n  dependencies:'
      for d in self.dependencies:
        result+= '\n    ' + d

    return result

  # Mark this lock entry as a prefix module and recurse on its dependencies
  def mark_prefix_recursive(self, lock_entries):
    self.prefix = True

    for d in self.dependencies:
      if d in lock_entries:
        lock_entries[d].mark_prefix_recursive(lock_entries)

  # Install the local files for this module
  def install(self, manifest, registry, verbose):
    if verbose: print('Installing module: ' + self.module_name)

    # Pull the module information from the registry
    try:
      if verbose: print('    Pulling module information from registry...')
      p = registry.module(self.module_name)

      # Make the directories as required
      if verbose: print('    Creating module installation directories...')
      if not IOOperations.directory_exists(manifest.module_directory):
        IOOperations.mkdir(manifest.module_directory)

      if not IOOperations.directory_exists(os.path.join(manifest.module_directory,
          self.module_name)):
        IOOperations.mkdir(os.path.join(manifest.module_directory, self.module_name))

      if not IOOperations.directory_exists(os.path.join(manifest.module_directory,
          self.module_name, self.resolved)):
        IOOperations.mkdir(os.path.join(manifest.module_directory,
          self.module_name, self.resolved))

      # Get the list of files for this module and version
      files = p.get_version_files(self.resolved)
      for url in files:
        fname = url.split('/')[-1]
        if verbose: print('    Installing ' + fname + '...')
        path = os.path.join(manifest.module_directory, self.module_name, self.resolved, fname)

        data = registry.get(url)

        with open(path, 'w') as f:
          f.write(data)
      if verbose: print('  Done.')

    except Exception as e:
      print('ERROR: ' + str(e))

  # Create a new lock entry from the given module file
  @staticmethod
  def from_module(mdl):
    le = LockEntry()

    le.module_name = mdl.module_name
    le.min_version = Dependency.version_string(mdl.min_version)
    le.max_version = Dependency.version_string(mdl.max_version)
    le.resolved = mdl.get_install_version()
    le.prefix = mdl.prefix
    le.dependencies = [d.module_name for d in mdl.dependencies]

    return le


# A class to represent the lock file
class Lockfile:
  # Create a new blank Lockfile
  def __init__(self):
    self.prefix = []
    self.list = []

  # Check if this Lockfile already contains the given module
  def contains(self, mdl):
    for le in self.prefix:
      if le.module_name == mdl:
        return True

    for le in self.list:
      if le.module_name == mdl:
        return True

    return False

  # Check if this Lockfile contains the given prerequisites for a module
  def has_prereqs(self, entry):
    for d in entry.dependencies:
      if not self.contains(d):
        return False

    return True

  # Adds the provided entry into the lock file
  def add(self, entry):
    if not self.has_prereqs(entry):
      raise ValueError('Module prerequisites not met: ' + entry.module_name)

    if self.contains(entry.module_name):
      return

    if entry.prefix:
      self.prefix.append(entry)
    else:
      self.list.append(entry)

  # Serialize this lockfile to a string
  def serialize(self):
    result = '# THIS FILE IS GENERATED AUTOMATICALLY. DO NOT MODIFY DIRECTLY.'

    # Add prefix modules if required
    if len(self.prefix) > 0:
      for le in self.prefix:
        result+= '\n\n' + le.serialize()

      result+= '\n\n' + ('-' * 50)

    # Add remaining modules
    for le in self.list:
      result+= '\n\n' + le.serialize()

    return result

  # Write this lockfile to the specified path
  def write(self, path):
    with open(path, 'w') as f:
      f.write(self.serialize())

  # Load a new Lockfile object from a serialized string
  @staticmethod
  def loads(representation):
    lf = Lockfile()
    data = representation.split('\n')
    working = None

    for line in data:
      # If this is a comment, ignore it
      if line.startswith('#'):
        continue

      # If this is an empty line, add the working module to the list
      elif len(line) < 1 and working is not None:
        lf.list.append(working)
        working = None

      # If this is a prefix separator, update accordingly
      elif line == ('-'*50):
        lf.prefix = lf.list
        lf.list = []
        for p in lf.prefix:
          p.prefix = True

      # If this is a new module entry, update the working
      elif not line.startswith('  ') and line.endswith(':'):
        if working is not None:
          raise ValueError('Improperly formatted Lockfile')
        working = LockEntry()
        working.module_name = line[:-1]

      # If this line contains version info, update working module
      elif line.startswith('  min_version: '):
        if working is None:
          raise ValueError('Improperly formatted Lockfile')
        working.min_version = line[15:]
      elif line.startswith('  max_version: '):
        if working is None:
          raise ValueError('Improperly formatted Lockfile')
        working.max_version = line[15:]
      elif line.startswith('  resolved: '):
        if working is None:
          raise ValueError('Improperly formatted Lockfile')
        working.resolved = line[12:]

      # If this line is a dependecies line, update accordingly
      elif line == '  dependencies:':
        continue
      elif line.startswith(' '*4):
        if working is None:
          raise ValueError('Improperly formatted Lockfile')
        working.dependencies.append(line[4:])

    # Add the final module to the Lockfile if necessary
    if working is not None:
      lf.list.append(working)

    return lf

  # Load the Lockfile from the provided path
  @staticmethod
  def loadf(path):
    with open(path,'r') as f:
      return Lockfile.loads(f.read())


# A class to handle the Manifest module resolution logic
class Resolver:
  # Create a new manifest module resolver
  def __init__(self, manifest, registry, quiet=False):
    self.manifest = manifest
    self.registry = registry
    self.quiet = quiet
    self.staging = None
    self.lock_staging = None

  # Recursively add this module's dependencies to the staging list
  def _stage(self, dependency):
    if self.staging is None:
      return

    if not dependency.module_name in self.staging:
      # Pull the info for this module if we haven't done so already
      try:
        entry = self.registry.module(dependency.module_name)
        self.staging[dependency.module_name] = entry
      except:
        if not self.quiet:
          print('Unable to find registry information for module: ' + dependency.module_name)
        return

      # Recurse on all module dependencies
      for d in entry.dependencies:
       self._stage(d)

    # Add dependency restrictions
    self.staging[dependency.module_name].add_constraint(dependency)

  def resolve(self):
    # Recursively stage each dependency module and add constraints
    self.staging = {}
    for d in self.manifest.dependencies:
      self._stage(d)

    # Ensure at least one valid candidate version for each module
    error = False
    for mdl in self.staging:
      v = self.staging[mdl].get_install_version()
      if v is None:
        if not self.quiet:
          print('No viable installation candidate found for module: ' + mdl)
        error = True
    if error:
      return None

    # Turn these modules into lock objects
    self.lock_staging = {}
    for mdl in self.staging:
      self.lock_staging[mdl] = LockEntry.from_module(self.staging[mdl])

    # Flag any prefix modules as such
    for le in self.lock_staging:
      if self.lock_staging[le].prefix:
        self.lock_staging[le].mark_prefix_recursive(self.lock_staging)

    # Create the Lockfile object
    lf = Lockfile()

    # Add modules to the Lockfile in installation order
    loopcount = 0
    while len(self.lock_staging) > 0:
      # If we have hit the max loop count, exit
      loopcount+= 1
      if loopcount > MAX_LOOP_COUNT:
        raise OverflowError('Hit max loop count generating Lockfile, possible circular reference.')

      # Loop over the remaining modules
      added = []
      for mdl in self.lock_staging:
        # If this module is already in the lockfile, skip it
        if lf.contains(mdl):
          added.append(mdl)
          continue

        # If this module has all prereqs met, add it
        if lf.has_prereqs(self.lock_staging[mdl]):
          lf.add(self.lock_staging[mdl])
          added.append(mdl)

      # Remove modules added to the lockfile this pass
      for p in added:
        del self.lock_staging[p]

    # Return the Lockfile
    return lf


# A class of utility methods to work with CSS and javascript
class Scripts:

  # Do a basic minification pass on Javascript
  # TODO: This assumes already minified Javascript, make this more robust?
  @staticmethod
  def minify_js(script):
    # Remove line breaks and tabs
    result = script.replace('\n',' ').replace('\t',' ')

    # Remove duplicate whitespace
    while '  ' in result:
      result = result.replace('  ',' ')

    # Remove leading and trailing whitespace
    result = result.strip()

    return result

  # Do a basic minification pass on CSS
  # TODO: This assumes already minified CSS, make this more robust?
  @staticmethod
  def minify_css(script):
    # Remove line breaks and tabs
    result = script.replace('\n','').replace('\t','')

    # Remove duplicate whitespace
    while '  ' in result:
      result = result.replace('  ',' ')

    # Remove leading and trailing whitespace
    result = result.strip()

    return result

  # Builds a module to return the compiled code
  # Returns a string list of the form [js, css, prefix]
  @staticmethod
  def build_module(manifest, mdl):
    result = ['','','']

    # Build the module path
    path = os.path.join(manifest.module_directory,
      mdl.module_name,
      mdl.resolved)

    # Compile the JS code into a single string
    js_files = IOOperations.get_files_of_type(path, '.js')
    js_contents = []
    for jsf in js_files:
      with open(os.path.join(path, jsf), 'r') as f:
        js_contents.append(f.read())
    built_js = ' '.join([Scripts.minify_js(d) for d in js_contents])

    # Output as JS or prefix as appropriate
    if mdl.prefix:
      result[2] = built_js
    else:
      result[0] = built_js

    # Compile the CSS code into a single string
    css_files = IOOperations.get_files_of_type(path, '.css')
    css_contents = []
    for cssf in css_files:
      with open(os.path.join(path, cssf), 'r') as f:
        css_contents.append(f.read())
    result[1] = ''.join([Scripts.minify_css(d) for d in css_contents])

    return result


# ------------------------------------------------------------------------
# USAGE INFORMATION METHODS

# Display the general usage information
def general_usage():
  print('\nUsage: python ' + sys.argv[0] + ' [options] <command> [parameters]')

  print('\nOptions in:')
  print('\t--help\t\tDisplay the usage information for the given command.')
  print('\t--version\tDisplay the version of this tool.')

  print('\nPossible commands:')
  print('\tadd <mdl> <ver>\tAdds the given module as a project dependency.')
  print('\tbuild\t\tCompile the module dependencies into a single file.')
  print('\thelp\t\tDisplay this usage info.')
  print('\tinit\t\tInitialize a new manifest file in the current directory.')
  print('\tinstall\t\tPull the required modules and save them locally.')
  print('\trm <mdl>\tRemoves the given module as a project dependency.')
  print('\trun\t\tEquivalent to running: resolve -> install -> build.')
  print('\tresolve\t\tResolve the project dependencies and build the lockfile.')
  print('')

# Display the init command usage information
def init_usage():
  print('\nUsage: python '+sys.argv[0]+' init')
  print('\nPurpose: Initialize a new project manifest file in the current directory.\n')

# Display the add command usage information
def add_usage():
  print('\nUsage: python '+sys.argv[0]+' add <module-name> <version-string>')
  print('\nPurpose: Add the specified module as a dependency in the project manifest.')
  print('<module-name>\t\tThe name of the module to include.')
  print('<version-string>\tThe string corresponding to the required version. An specific version will be matched exactly. A version prefixed with \'+\' will install the latest possible version. A version string prefixed with \'^\' will install the latest possible release without changing the major version.\n')

# Display the rm command usage information
def rm_usage():
  print('\nUsage: python '+sys.argv[0]+' rm <module-name> [-f]')
  print('\nPurpose: Remove a dependency module from the project manifest.')
  print('If the \'-f\' option is included, it will also delete any local files related to that module.\n')

# Display the resolve command usage information
def resolve_usage():
  print('\nUsage: python '+sys.argv[0]+' resolve [-q]')
  print('\nPurpose: Resolve all the dependency module constraints to determine the desired module version and order.')
  print('If the \'-q\' option is included, module resolution errors will be supressed.\n')

# Display the install command usage information
def install_usage():
  print('\nUsage: python '+sys.argv[0]+' install [-v]')
  print('\nPurpose: Download all required module files and save them locally.')
  print('If the \'-v\' option is included, verbose output will be generated.\n')

# Display the build command usage information
def build_usage():
  print('\nUsage: python '+sys.argv[0]+' build')
  print('\nPurpose: Compile all the required modules into unified output files.\n')

# Display the run command usage information
def run_usage():
  print('\nUsage: python '+sys.argv[0]+' run')
  print('\nPurpose: Executes the entire install and build pipeline.')
  print('Equivalent to running the \'resolve\', \'install\', and \'build\' commands in succession.\n')

# ------------------------------------------------------------------------
# CORE COMMAND METHODS

# Print the version information of this script
def version():
  print('Bantam Coop ' + VERSION)

# Print the usage information of this script
def help(command = None):
  if command == None or command == 'help':
    return general_usage()
  elif command == 'init':
    return init_usage()
  elif command == 'add':
    return add_usage()
  elif command == 'rm':
    return rm_usage()
  elif command == 'resolve':
    return resolve_usage()
  elif command == 'install':
    return install_usage()
  elif command == 'build':
    return build_usage()
  elif command == 'run':
    return run_usage()
  else:
    print('\nCommand not recognized.')
    return general_usage()

# Implements the init command
def init(params):
  if IOOperations.file_exists(MANIFEST_PATH):
    print('Manifest file already exists at this location.')
  else:
    m = Manifest()
    m.write(MANIFEST_PATH)

# Implements the add command
def add(module, version, params):
  if not IOOperations.file_exists(MANIFEST_PATH):
    print('No manifest file found. Use the \'init\' command to create one.')
  else:
    try:
      m = Manifest.loadf(MANIFEST_PATH)

      d = Dependency(module, version)
      m.dependencies.append(d)

      m.write(MANIFEST_PATH)
    except json.decoder.JSONDecodeError:
      print('ERROR: Corrupted manifest file. Unable to parse.')
    except IOError:
      print('ERROR: Unable to write to manifest file.')

# Implements the rm command
def rm(module, params):
  if not IOOperations.file_exists(MANIFEST_PATH):
    print('No manifest file found. Use the \'init\' command to create one.')
  else:
    # Remove from the manifest file
    try:
      m = Manifest.loadf(MANIFEST_PATH)

      indices = [i for i,d in enumerate(m.dependencies)
        if d.module_name == module]

      if len(indices) > 0:
        indices.reverse()
        for i in indices:
          del m.dependencies[i]

      m.write(MANIFEST_PATH)

      # Remove the local files if needed
      if ('-f' in params
          and IOOperations.directory_exists(m.module_directory)):
        if IOOperations.directory_exists(
            os.path.join(m.module_directory, module)):
          try:
            IOOperations.rmdir(os.path.join(m.module_directory, module),
              recursive=True)
          except (OSError, FileNotFoundError) as e:
            print('ERROR: Unable to remove local module files.')

    except json.decoder.JSONDecodeError:
      print('ERROR: Corrupted manifest file. Unable to parse.')
    except IOError:
      print('ERROR: Unable to write to manifest file.')

# Implements the resolve command
def resolve(params, registry=None):
  QUIET = '-q' in params

  if not IOOperations.file_exists(MANIFEST_PATH):
    print('No manifest file found. Use the \'init\' command to create one.')
  else:
    try:
      m = Manifest.loadf(MANIFEST_PATH)
      if registry is None:
        r = Registry(REGISTRY_PATH)
      else:
        r = registry

      resolver = Resolver(m, r, quiet = QUIET)

      lock = resolver.resolve()

      if lock is None:
        if not QUIET:
          print('ERROR: Error generating lock file')
      else:
        lock.write(LOCK_PATH)

    except json.decoder.JSONDecodeError:
      print('ERROR: Corrupted manifest file. Unable to parse.')
    except IOError:
      print('ERROR: Unable to write to manifest file.')
    except OverflowError:
      if not QUIET:
        print('ERROR: Hit max loop count generating lockfile, possible circular reference.')

# Implements the install command
def install(params, registry=None):
  VERBOSE = '-v' in params

  if not IOOperations.file_exists(MANIFEST_PATH):
    print('No manifest file found. Use the \'init\' command to create one.')
  elif not IOOperations.file_exists(LOCK_PATH):
    print('No lockfile found. Use the \'resolve\' command to create one.')
  else:
    try:
      m = Manifest.loadf(MANIFEST_PATH)

      lf = Lockfile.loadf(LOCK_PATH)
      if registry is None:
        r = Registry(REGISTRY_PATH)
      else:
        r = registry

      for le in lf.prefix:
        le.install(m, r, VERBOSE)

      for le in lf.list:
        le.install(m, r, VERBOSE)

    except Exception as e:
      print('ERROR: ' + str(e))

# Implements the build command
def build(params):
  if not IOOperations.file_exists(MANIFEST_PATH):
    print('No manifest file found. Use the \'init\' command to create one.')
  elif not IOOperations.file_exists(LOCK_PATH):
    print('No lockfile found. Use the \'resolve\' command to create one.')
  else:
    try:
      # Load the manifest and lock files
      m = Manifest.loadf(MANIFEST_PATH)
      lf = Lockfile.loadf(LOCK_PATH)

      js = []
      css = []
      prefix = []

      # Loop through the prefix modules in order and build them
      for mdl in lf.prefix:
        results = Scripts.build_module(m, mdl)
        if len(results[0]) > 0:
          js.append(results[0])
        if len(results[1]) > 0:
          css.append(results[1])
        if len(results[2]) > 0:
          prefix.append(results[2])

      # Loop through the non-prefix modules in order and build them
      for mdl in lf.list:
        results = Scripts.build_module(m, mdl)
        if len(results[0]) > 0:
          js.append(results[0])
        if len(results[1]) > 0:
          css.append(results[1])
        if len(results[2]) > 0:
          prefix.append(results[2])

      # Write the output files
      with open(m.js_target, 'w') as f:
        f.write(' '.join(js))
      with open(m.css_target, 'w') as f:
        f.write(''.join(css))
      with open(m.prefix_target, 'w') as f:
        f.write(' '.join(prefix))

    except Exception as e:
      print('ERROR: ' + str(e))

# Implements the run command
def run(params, registry=None):
  resolve(params, registry=registry)
  install(params, registry=registry)
  build(params)

# ------------------------------------------------------------------------
# MAIN SCRIPT ENTRY

# The main entry point into the script
def main():

  # If too few arguments, display the general usage info
  if len(sys.argv) < 2:
    return help()

  # Parse the options, commands, and parameters
  options = [v.lower() for v in sys.argv if v.startswith('--')]
  commands = [v.lower() for v in sys.argv if not v.startswith('-')][1:]
  params = [v.lower() for v in sys.argv if v.startswith('-')
    and not v.startswith('--')]

  # Handle the global options
  #   Handle the --version option
  if '--version' in options:
    return version()
  #   Handle the --help option
  elif '--help' in options:
    if len(commands) < 1:
      return help()
    else:
      return help(command = commands[0])

  # If not command passed at this point, display usage
  if len(commands) < 1:
    return help()

  # Call the appropriate method based on the passed command
  if commands[0] == 'init':
    return init(params)
  elif commands[0] == 'add':
    if len(commands) < 3:
      return help(command = 'add')
    else:
      return add(commands[1], commands[2], params)
  elif commands[0] == 'rm':
    if len(commands) < 2:
      return help(command = 'rm')
    else:
      return rm(commands[1], params)
  elif commands[0] == 'resolve':
    return resolve(params)
  elif commands[0] == 'install':
    return install(params)
  elif commands[0] == 'build':
    return build(params)
  elif commands[0] == 'run':
    return run(params)
  elif commands[0] == 'help':
    return help()
  else:
    print('Command not recognized. Use the --help option for more information.')
    return

# If this is not an import, call the main entry point of the script
if __name__ == '__main__':
  main()
