# Bantam Coop

> "The standard library saves programmers from having to reinvent the wheel."  
> -- Bjarne Stroustrup, creator of C++

> "In the old days, you would chastise people for reinventing the wheel. Now we beg, 'Oh, please, please reinvent the wheel.'"  
> -- Alan Kay, father of object-oriented programming

## What is Coop?

Coop is a module manager for the [BantamJS](https://canderegg.gitlab.io/bantam-js) javascript library. Because Bantam is so lightweight, the functionality of the core library is pretty limited. To implement more advanced functionality, additional modules were created on top of BantamJS.

Coop provides a way to manage all these Bantam modules. Because each module is designed to be small and independent, you include only the functionality you actually use for your app.

## You wrote a new package manager? Weren't there enough of those already?

Apparently not.

## But the whole point of BantamJS was to get away from dependency bloat and massive convention-over-configuration frameworks!

Yes. A few points to consider here:

1. Bantam modules are small. The core Bantam library is 4 kilobytes. Most modules in the registry are smaller than that. You can install twenty Bantam modules and still be lighter-weight than basic jQuery, over a hundred without surpassing Angular. Small and lightweight is *still* the name of the Bantam game.
2. Coop (and the Bantam ecosystem generally) values configuration over convention. Want to change the module directory, or how the output files get linked into your app? You can. Want to run Coop as part of your deployment process? Go for it. Want to run it in your development environment and check in the compiled files? Also an option.
3. Chaining dependencies is discouraged in Bantam modules. In NPM it's not unusual to have packages that depend on packages that depend on packages a dozen levels deep. In a normal Bantam module two levels is pushing it. You might need twenty modules for a highly ambitious project, but you should never need thousands.

Bantam is still geared toward agile DIY, Coop just gives you a variety of standard tools to use. You're still building your own house, just not forging your own nails or milling your own two-by-fours.

## Still available for commercial use?

Yep, all distributed under the same [MIT License](LICENSE) as the core BantamJS library.

## Link me to the docs?

We [got you](https://canderegg.gitlab.io/bantam-coop/docs.html).
