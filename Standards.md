# Bantam Coop Standard Specifications

- [Registry Files](#registry-files)
- [Manifest Files](#manifest-files)
- [Lock Files](#lock-files)

## Registry Files

Two files will need to be modified to list a new module (or a new version of a module) on the Coop registry. First, the name of the module needs to be included in the `modules.json` file on the registry server. Module names should be lowercase, and contain alphanumeric characters and hyphens only. This change allows the module to be indexed for the registry documentation site.

Next, the registry file needs to be added in the `module` directory of the registry site. This registry file is a JSON file with the same name as the module itself. This provides the metadata about the module, which allows it to be managed and installed by Coop.

The registry file takes the following format:

```json
{
  "module-name": "example",
  "module-desc": "A human-readable description of the purpose of the module.",
  "author": "Author's Name",
  "documentation": "http://docs.example.com",
  "repository": "http://author.gitlab.com/example-module.git",
  "license": "http://author.gitlab.com/example-module/LICENSE",
  "prefix": false,
  "dependencies": {
    "dependency-one": "+1.0.0",
    "dependency-two": "^2.5.5",
    "dependency-three": "1.7.26"
  },
  "versions": {
    "1.0": [
      "http://author.gitlab.com/example-module/example-1.0.min.js",
      "http://author.gitlab.com/example-module/example-1.0.min.css",
      "http://author.gitlab.com/example-module/LICENSE"
    ],
    "1.1": [
      "http://author.gitlab.com/example-module/example-1.1.min.js",
      "http://author.gitlab.com/example-module/example-1.1.min.css",
      "http://author.gitlab.com/example-module/LICENSE"
    ],
    "1.2": [
      "http://author.gitlab.com/example-module/example-1.2.min.js",
      "http://author.gitlab.com/example-module/example-1.2.min.css",
      "http://author.gitlab.com/example-module/LICENSE"
    ]
  },
  "changelog": {
    "1.0": "Initial module version.",
    "1.1": "A summary of what changed in version 1.1",
    "1.2": "A summary of what changed in version 1.2"
  }
}
```

The fields are populated as follows:

`module-name` the unique, machine-usable name of the module.

`module-desc` a sentence or two describing what the module does and why it's useful. This is designed to give a prospective user the gist of the module. Keep it at a paragraph or less.

`author` the name of this module's author. If there are multiple contributors, all names should be listed separated by commas.

`documentation` the URL of the documentation page for this module. Documentation should go into detail about the functionality and usage of the module.

`repository` the URL of the source repository of the module.

`license` the URL of the license information of the module. Modules that are not distributed under the MIT license will be subject to stricter review before inclusion in the registry.

`prefix` whether this module needs to be included in prefix compilation, to be processed before page load.

`dependencies` is a dictionary of dependency modules. The key for each entry will be the module name of the dependency, the value for each entry will be the version string of the version required for the dependency. If the version string is specified exactly, Coop will attempt to install that exact version. If the version string is prefixed with a `+`, Coop will attempt to install the latest possible version of that module (greater than the specified version). If the version string is prefixed with a `^`, Coop will attempt to install the latest possible revision of that major version.

`versions` is a dictionary specifying the available version of the module. For each version, the key will be the version string, and the value will be an array of URLs to each file included in that version of the module. If minified versions of the module files are available, these should link to the minified versions. Once a module version has been listed on the registry, that version and the associated files should never be removed to preserve backwards compatibility. Comitting and hosting module files within the Coop repository is *strongly* encouraged.

`changelog` is a dictionary of the patch notes for each released version.

## Manifest Files

Manifest files maintain a list of the configuration settings for a project, as well as the list of dependency modules and desired version for each module. This information is housed in a `manifest.json` file at the root level of the project.

The file will have the following format:

```json
{
  "module_directory": "bantam_modules",
  "js_target": "scripts/dependencies.js",
  "css_target": "styles/dependencies.css",
  "prefix_target": "scripts/prefix-dependencies.js",
  "dependencies": {
    "dependency-one": "+1.0.0",
    "dependency-two": "^2.5.5",
    "dependency-three": "1.7.26"
  }
}
```

The fields are populated as follows:

`module_directory` is the name of the folder in which Coop should install the files for all the required Bantam modules.

`js_target` is the path of the file that all the javascript dependencies should be compiled to. Your app should link to this file at the end of the `body` section.

`css_target` is the path of the file that all the CSS dependencies should be compiled to. Your app should link to this file in the `head` section.

For the rare cases where a javascript file needs to be loaded before the rest of the document, `prefix_target` specifies the file path for those modules to be compiled to. Your app should link this file in the `head` section.

`dependencies` is a dictionary of project dependencies. The key for each entry will be the module name of the dependency, the value for each entry will be the version string of the version required for the dependency. If the version string is specified exactly, Coop will attempt to install that exact version. If the version string is prefixed with a `+`, Coop will attempt to install the latest possible version of that module (greater than the specified version). If the version string is prefixed with a `^`, Coop will attempt to install the latest possible revision of that major version.

## Lock Files

The lock file is programatically generated when resolving the dependencies and versions required. It will take the following format behind the scenes:

```yml
# THIS FILE IS AUTOMATICALLY GENERATED. DO NOT MODIFY DIRECTLY.

module2:
  min_version: 1.0.0
  max_version: null
  resolved: 2.7.6

module1:
  min_version: 1.2.7
  max_version: 3.4.2
  resolved: 3.4.2
  dependencies:
    module2
```

Each module dependency is listed by name, along with the minimum constrained version, the maximum constrained version, and the resolved version that satisfies all criteria and will actually be installed.

If a module depends on other modules, those modules will be listed as dependencies. The order of modules in the lock file is such that each module comes after all required dependencies.

The lock file is generated according to the following algorithm.

1. Loop over the list of required modules and recursively process their dependencies to generate a list of the modules required and their candidate versions.
2. Loop over all those modules to adjust minimum and maximum versions, and resolve those to a best-fit candidate version.
3. While there are modules that have not yet been written to the lock file, loop over the list of modules that have not yet been written to the lock file, writing any for which all dependencies are satisfied.

The lock file is a human-auditable intermediate artifact of the module resolution process. It will be used to install any required modules, and when determining the order in which modules are compiled into the output.
