(function(global) {
  let context = {};

  // Get the module index
  context.getIndex = function() {
    $bt.ajax('modules.json', {
      method: 'GET',
      headers: {'Accept': 'application/json'},
      success: context.parseIndex,
      failure: function(data) { console.log(data); }
    });
  };

  // Turn a list of modules into an index
  context.parseIndex = function(data) {

    // Parse and sort the list of modules
    context.index = [];
    for (let key in data) {
      if (data.hasOwnProperty(key)) {
        context.index.push(key);
      }
    }
    context.index.sort();

    // Generate an action link for each
    let html = '';
    let linkTemplate = '<a href="#{name}" class="registry-link link-{name}" onclick="app.getModule(\'{name}\');"><span class="code">{name}</span></a><br>';
    for (let i=0; i<context.index.length; i++) {
      html+= $bt.render(linkTemplate, {'name': context.index[i]});
    }

    // Write these action links into the sidebar
    let sidebar = $bt.get('#registry-index');
    sidebar.innerHTML = html;
    
    // Pull the default module information
    if (window.location.hash) {
      let hash = window.location.hash.substr(1);
      context.getModule(hash);
    } else {
      context.getModule('bantam');
    }
  };

  // Show the spinner
  context.showSpinner = function() {
    $bt.get('#registry-content').add('transparent');
    $bt.get('#spinner').erase('transparent');
  };

  // Hide the spinner
  context.hideSpinner = function() {
    $bt.get('#spinner').add('transparent');
    $bt.get('#registry-content').erase('transparent');
  };

  // Get module information from the registry
  context.getModule = function(name) {
    // Show the spinner
    context.showSpinner();

    // Set the active link
    $bt.map($bt.get('.registry-link'), function(e) { e.erase('active'); });
    $bt.get('.link-' + name)[0].add('active');

    // Set the window hash
    window.location.hash = '#' + name;

    // Pull the module data
    $bt.ajax('module/' + name + '.json', {
      method: 'GET',
      headers: {'Accept': 'application/json'},
      success: context.parseModule,
      failure: function(data) { console.log(data); }
    });
  };

  // Parse the module information
  context.parseModule = function(data) {
    let template = '<h2><span class="code">{module-name}</span></h2><p>{module-desc}</p><table><th>Author(s):</th><td>{author}</td></tr><tr><th>Documentation:</th><td><a href="{documentation}" target="_blank">{documentation}</td></tr><tr><th>Repository:</th><td><a href="{repository}" target="_blank">{repository}</a></td></tr><tr><th>License:</th><td><a href="{license}" target="_blank">{license}</a></td></tr><tr><th>Prefix:</th><td>{prefix-string} <div class="hint" title="Prefix scripts are run before the page body loads.">?</div></td></tr>';
    let dependencyTemplate = '{dependency}<br>';
    let versionTemplate = '<tr><th></th><td>{version}</td></tr>';

    // Determine the prefix information
    if (data.prefix) {
      data['prefix-string'] = 'Yes';
    } else {
      data['prefix-string'] = 'No';
    }

    // Render basic module information
    let html = '';
    html+= $bt.render(template, data);

    // Render dependencies
    dependencies = [];
    for (let key in data.dependencies) {
      if (data.dependencies.hasOwnProperty(key)) {
        dependencies.push(key+'@'+data.dependencies[key]);
      }
    }
    dependencies.sort();
    if (dependencies.length > 0) {
      html+= '<tr><th>Module Dependencies:</th><td>';
      for (let i=0; i<dependencies.length; i++) {
        html+= $bt.render(dependencyTemplate, {'dependency': dependencies[i]});
      }
      html+= '</td></tr>';
    }

    // Render versions
    versions = [];
    for (let key in data.versions) {
      if (data.versions.hasOwnProperty(key)) {
        versions.push(key);
      }
    }
    versions.sort();
    versions.reverse();
    html += '<tr><th>Module Versions:</th><td>';
    for (let i=0; i<versions.length; i++) {
      html+= '<strong>v' + versions[i] + ':</strong> ';
      if (data.changelog && data.changelog[versions[i]]) {
        html += data.changelog[versions[i]];
      }
      html+= '<ul>';
      for (let j=0; j<data.versions[versions[i]].length; j++) {
        html+= '<li>' + data.versions[versions[i]][j].split('/').reverse()[0] + '</li>';
      }
      html += '</ul>';
    }
    html += '</td></tr>';

    html+= '</table>';
    $bt.get('#registry-content').innerHTML = html;

    context.hideSpinner();
  };

  // Initialize the registry app
  context.init = function() {
    context.getIndex();
  };

  global.app = context;
})(window);
app.init();
